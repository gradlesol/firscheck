package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gradlesol.firstcheck.adapter.detailsAdapter;
import com.gradlesol.firstcheck.fragment.homeFragment;
import com.gradlesol.firstcheck.fragment.savedFragment;
import com.gradlesol.firstcheck.fragment.submittedFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class my_recordsActivity extends AppCompatActivity implements View.OnClickListener {

    /*
    ListView listView;
    protected List<ParseObject> mRecord;

    String android_id;
*/
    TextView TextView_my_records_submitted,TextView_my_records_saved;
    savedFragment savedfragment;
    submittedFragment submittedfragment;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_records);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView_my_records_submitted = (TextView)findViewById(R.id.TextView_my_records_submitted);
        TextView_my_records_saved = (TextView)findViewById(R.id.TextView_my_records_saved);

        TextView_my_records_submitted.setOnClickListener(this);
        TextView_my_records_saved.setOnClickListener(this);

        savedfragment = new savedFragment();
        submittedfragment = new submittedFragment();
        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_my_records, submittedfragment);
        fragmentTransaction.commit();
/*
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        listView = (ListView)findViewById(R.id.listview);

        //show_list(false);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseObject recordObject = mRecord.get(position);
                String caseNo = recordObject.getString("caseNo");
                startActivity(new Intent(getApplicationContext(),my_records_details_Activity.class).putExtra("caseNo",caseNo));
            }
        });
*/


    }

    /*
    public void show_list(Boolean saved){
        if(isNetworkAvailable()){
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);
            query.whereEqualTo("username",android_id);
            query.orderByDescending("createdAt");
            query.whereEqualTo("saved",saved);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if(objects == null){
                        Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_LONG).show();
                    }else {
                        if (e == null) {
                            try {
                                mRecord = objects;
                                detailsAdapter adapter = new detailsAdapter(getApplicationContext(), mRecord);
                                listView.setAdapter(adapter);
                            } catch (NullPointerException n) {
                            }
                        } else {

                        }
                    }
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.TextView_my_records_submitted:
                TextView_my_records_submitted.setBackgroundResource(R.drawable.rounded_tab3);
                TextView_my_records_submitted.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_my_records_saved.setBackgroundResource(R.drawable.rounded_tab2);
                TextView_my_records_saved.setTextColor(getResources().getColor(R.color.greenButton));
                fm = getSupportFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_my_records, submittedfragment);
                fragmentTransaction.commit();
                //show_list(false);
                break;
            case R.id.TextView_my_records_saved :
                TextView_my_records_submitted.setBackgroundResource(R.drawable.rounded_tab1);
                TextView_my_records_submitted.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_my_records_saved.setBackgroundResource(R.drawable.rounded_tab4);
                TextView_my_records_saved.setTextColor(Color.parseColor("#FFFFFF"));
                fm = getSupportFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_my_records, savedfragment);
                fragmentTransaction.commit();
                //show_list(true);
                break;
            default:
                break;
        }
    }
}
