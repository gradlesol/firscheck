package com.gradlesol.firstcheck;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.onesignal.OneSignal;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class uploadingActivity extends AppCompatActivity {

    String caseNo,derm,name,dermuid,type,username,gender,age,howlong,description,region,ethnicity,code,label,insurance;
    String from;
    Boolean saved = true;
    Boolean myrecords = false;

    private DbHelper dbHelper;
    private Cursor cursor;

    byte[] photo1, photo2, photo3, molephoto;

    ProgressBar progressBar_first,progressBar_second,progressBar_skinscope,progressBar_mole;

    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploading);

        dbHelper = new DbHelper(getApplicationContext());
        cursor = dbHelper.selectRecords();

        saved = getIntent().getBooleanExtra("saved",true);

        if(!saved){
            derm = getIntent().getStringExtra("derm");
            name = getIntent().getStringExtra("name");
            dermuid = getIntent().getStringExtra("dermuid");
            myrecords = getIntent().getBooleanExtra("myrecords",false);
        }

        progressBar_first = (ProgressBar)findViewById(R.id.progressBar_first);
        progressBar_second = (ProgressBar)findViewById(R.id.progressBar_second);
        progressBar_skinscope = (ProgressBar)findViewById(R.id.progressBar_third);
        progressBar_mole = (ProgressBar)findViewById(R.id.progressBar_mole);

        LayoutInflater layoutInflater = LayoutInflater.from(uploadingActivity.this);
        View promptView = layoutInflater.inflate(R.layout.progress_layout, null);

        dialog = new Dialog(uploadingActivity.this, R.style.progressbar_style);
        dialog.setContentView(promptView);
        dialog.setCancelable(false);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.show();

        /*dialog = new ProgressDialog(uploadingActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
*/
        if(myrecords){
            caseNo = getIntent().getStringExtra("caseNo");
            uploading_data(false);
        }else {
            if (cursor.moveToFirst()) {
                do {
                    caseNo = cursor.getString(cursor.getColumnIndex("caseNo"));
                    gender = cursor.getString(cursor.getColumnIndex("gender"));
                    age = cursor.getString(cursor.getColumnIndex("age"));
                    howlong = cursor.getString(cursor.getColumnIndex("howlong"));
                    description = cursor.getString(cursor.getColumnIndex("description"));
                    region = cursor.getString(cursor.getColumnIndex("region"));
                    ethnicity = cursor.getString(cursor.getColumnIndex("ethnicity"));
                    code = cursor.getString(cursor.getColumnIndex("code"));
                    label = cursor.getString(cursor.getColumnIndex("label"));
                    insurance = cursor.getString(cursor.getColumnIndex("insurance"));
                    type = cursor.getString(cursor.getColumnIndex("type"));
                    username = cursor.getString(cursor.getColumnIndex("Username"));
                    photo1 = cursor.getBlob(cursor.getColumnIndex("photo1"));
                    photo2 = cursor.getBlob(cursor.getColumnIndex("photo2"));
                    photo3 = cursor.getBlob(cursor.getColumnIndex("photo3"));
                    molephoto = cursor.getBlob(cursor.getColumnIndex("molephoto"));
                    uploading_data(saved);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

    }

    public void uploading_data(Boolean saved){

        if(myrecords){
            ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
            query.whereEqualTo("caseNo", caseNo);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(final ParseObject object, ParseException e) {
                    object.put("saved",false);
                    OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                        @Override
                        public void idsAvailable(String userId, String registrationId) {
                            Log.d("debug", "User:" + userId);
                            object.put("uid", userId);
                            if (registrationId != null)
                                Log.d("debug", "registrationId:" + registrationId);
                        }
                    });
                    object.put("derm", derm);
                    object.put("name", name);
                    object.put("completed", false);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {

                                try {
                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'You have a new case from a patient.'}, 'include_player_ids': ['" + dermuid + "']}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                    dialog.dismiss();
                                    Intent intent = new Intent(getApplicationContext(), my_recordsActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();

                                } catch (JSONException j) {
                                }
                            }
                        }
                    });

                }
            });
        }else {
            final ParseObject object = new ParseObject(constants.TABLE_NAME);
            object.put("caseNo", caseNo);
            object.put("type", type);
            object.put("username", username);
            object.put("gender", gender);
            object.put("age", age);
            object.put("howlong", howlong);
            object.put("description", description);
            object.put("region", region);
            object.put("ethnicity", ethnicity);
            object.put("code", code);
            object.put("label", label);
            object.put("insurance", insurance);
            String partFilename = "photo1" + ".jpg";
            if (photo1 != null) {
                final ParseFile photoFile = new ParseFile(partFilename, photo1);
                photoFile.saveInBackground(new ProgressCallback() {
                    @Override
                    public void done(Integer percentDone) {
                        progressBar_first.setProgress(percentDone);
                    }
                });
                object.put("photo1", photoFile);
            }
            if (photo2 != null) {
                partFilename = "photo2" + ".jpg";
                final ParseFile photoFile = new ParseFile(partFilename, photo2);
                photoFile.saveInBackground(new ProgressCallback() {
                    @Override
                    public void done(Integer percentDone) {
                        progressBar_second.setProgress(percentDone);
                    }
                });
                object.put("photo2", photoFile);
            }
            if (photo3 != null) {
                partFilename = "photo3" + ".jpg";
                final ParseFile photoFile = new ParseFile(partFilename, photo3);
                photoFile.saveInBackground(new ProgressCallback() {
                    @Override
                    public void done(Integer percentDone) {
                        progressBar_skinscope.setProgress(percentDone);
                    }
                });
                object.put("photo3", photoFile);
            }
            if (molephoto != null) {
                partFilename = "molephoto" + ".jpg";
                final ParseFile photoFile = new ParseFile(partFilename, molephoto);
                photoFile.saveInBackground(new ProgressCallback() {
                    @Override
                    public void done(Integer percentDone) {
                        progressBar_mole.setProgress(percentDone);
                    }
                });
                object.put("molephoto", photoFile);
            }
            object.put("saved", saved);
            if(!saved){
                OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                    @Override
                    public void idsAvailable(String userId, String registrationId) {
                        Log.d("debug", "User:" + userId);
                        object.put("uid", userId);
                        if (registrationId != null)
                            Log.d("debug", "registrationId:" + registrationId);
                    }
                });
                object.put("derm", derm);
                object.put("name", name);
                object.put("completed", false);
            }
            object.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        try {
                            OneSignal.postNotification(new JSONObject("{'contents': {'en':'You have a new case from a patient.'}, 'include_player_ids': ['" + dermuid + "']}"),
                                    new OneSignal.PostNotificationResponseHandler() {
                                        @Override
                                        public void onSuccess(JSONObject response) {
                                            Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                        }

                                        @Override
                                        public void onFailure(JSONObject response) {
                                            Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                        }
                                    });
                            dialog.dismiss();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } catch (JSONException j) {
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
