package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.adapter.ExpandableListAdapter;
import com.gradlesol.firstcheck.adapter.dermatologistAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class dermatolologistActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_dermatologist_continue;

    String derm="",Name="",dermuid,region="",caseNo="",from="";

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> tempDataHeader;
    HashMap<String, List<ParseUser>> listDataChild;
    Boolean avail = false;
    Boolean myrecords = false;

    List<ParseUser> r0 = new ArrayList<ParseUser>();
    List<ParseUser> r1 = new ArrayList<ParseUser>();
    List<ParseUser> r2 = new ArrayList<ParseUser>();
    List<ParseUser> r3 = new ArrayList<ParseUser>();
    List<ParseUser> r4 = new ArrayList<ParseUser>();
    List<ParseUser> r5 = new ArrayList<ParseUser>();
    List<ParseUser> r6 = new ArrayList<ParseUser>();
    List<ParseUser> r7 = new ArrayList<ParseUser>();
    List<ParseUser> r8 = new ArrayList<ParseUser>();
    List<ParseUser> r9 = new ArrayList<ParseUser>();
    List<ParseUser> r10 = new ArrayList<ParseUser>();
    List<ParseUser> r11 = new ArrayList<ParseUser>();
    List<ParseUser> r12 = new ArrayList<ParseUser>();
    List<ParseUser> r13 = new ArrayList<ParseUser>();
    List<ParseUser> r14 = new ArrayList<ParseUser>();
    List<ParseUser> r15 = new ArrayList<ParseUser>();
    List<ParseUser> r16 = new ArrayList<ParseUser>();
    List<ParseUser> r17 = new ArrayList<ParseUser>();

    String[] strings = {
            "Northland Region",
            "Auckland Region",
            "Walkato Region",
            "Bay of Plenty Region",
            "Gisborne Region",
            "Hawke's Bay Region",
            "Taranaki Region",
            "Manawatu-Wanganui Region",
            "Wellington Region",
            "Tasman Region",
            "Nelson Region",
            "Marlborough Region",
            "West Coast Region",
            "Canterbury Region",
            "Otago Region",
            "Southland Region",
            "Chatham Islands Territory",
            "Area Outside Territorial Authority"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dermatolologist);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        region = getIntent().getStringExtra("region");
        myrecords = getIntent().getBooleanExtra("myrecords",false);
        if(myrecords){
            caseNo = getIntent().getStringExtra("caseNo");
        }

        Arrays.sort(strings);


        if(region != null){
            String[] temp = new String[strings.length];
            int p = 0;
            for(int i = 0; i<temp.length ; i++){
                if(i == 0){
                    temp[0] = region;
                }else {
                    if(region.equals(strings[p])){
                        p++;
                        i--;
                    } else {
                        temp[i] = strings[p];
                        p++;
                    }
                }
            }
            strings = temp;
        }

        expListView = (ExpandableListView) findViewById(R.id.lvExp_dermatologist);
        prepareListData();

        Button_dermatologist_continue = (Button)findViewById(R.id.Button_dermatologist_continue);
        Button_dermatologist_continue.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Button_dermatologist_continue:
                if(Name == null){
                    Toast.makeText(getApplicationContext(),"Please Select Dermatologist",Toast.LENGTH_LONG).show();
                } else {
                    if(myrecords) {
                            startActivity(new Intent(getApplicationContext(), paymentActivity.class)
                                    .putExtra("derm", derm)
                                    .putExtra("name", Name)
                                    .putExtra("dermuid", dermuid)
                                    .putExtra("caseNo", caseNo)
                                    .putExtra("myrecords", true)
                            );
                    }else {
                        startActivity(new Intent(getApplicationContext(), paymentActivity.class)
                                .putExtra("derm", derm)
                                .putExtra("name", Name)
                                .putExtra("dermuid", dermuid)
                        );
                    }
                }
                break;
            default:
                break;

        }
    }

    private Boolean prepareListData() {
        listDataHeader = new ArrayList<String>();
        tempDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<ParseUser>>();
        if(setData()){
            return true;
        }else {
            return false;
        }
    }

    public Boolean setChild(){
        Collections.sort(listDataHeader);
        if(avail){
            tempDataHeader.add(region.toUpperCase());
            for(int i=0 ;i < listDataHeader.size(); i++ ){
                if((region).equalsIgnoreCase(listDataHeader.get(i))){

                }else {
                    tempDataHeader.add(listDataHeader.get(i));
                }
            }
            listDataHeader = tempDataHeader;
        }

        int n = 0;
        for(int i=0; i<18; i++){
            if(n < listDataHeader.size()) {
                if ((i == 0) && (r0.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r0);
                    n++;
                } else if ((i == 1) && (r1.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r1);
                    n++;
                } else if ((i == 2) && (r2.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r2);
                    n++;
                } else if ((i == 3) && (r3.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r3);
                    n++;
                } else if ((i == 4) && (r4.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r4);
                    n++;
                } else if ((i == 5) && (r5.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r5);
                    n++;
                } else if ((i == 6) && (r6.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r6);
                    n++;
                } else if ((i == 7) && (r7.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r7);
                    n++;
                } else if ((i == 8) && (r8.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r8);
                    n++;
                } else if ((i == 9) && (r9.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r9);
                    n++;
                } else if ((i == 10) && (r10.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r10);
                    n++;
                } else if ((i == 11) && (r11.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r11);
                    n++;
                } else if ((i == 12) && (r12.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r12);
                    n++;
                } else if ((i == 13) && (r13.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r13);
                    n++;
                } else if ((i == 14) && (r14.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r14);
                    n++;
                } else if ((i == 15) && (r15.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r15);
                    n++;
                } else if ((i == 16) && (r16.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r16);
                    n++;
                } else if ((i == 17) && (r17.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r17);
                    n++;
                } else {
                    //do nothing
                }
            }
            if(i == 17){
                listAdapter = new ExpandableListAdapter(getApplicationContext(), listDataHeader, listDataChild);
                expListView.setAdapter(listAdapter);
                for(int l=0; l < listAdapter.getGroupCount(); l++)
                    expListView.expandGroup(l);

                expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        return true;
                    }
                });
                expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        ParseUser user = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                        Name = user.getString("Name");
                        derm = user.getUsername();
                        dermuid = user.getString("uid");
                        return false;
                    }
                });
            }
        }

        return true;
    }

    public Boolean setData(){
        if(isNetworkAvailable()){
            try {
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        if (e == null) {
                            try {
                                if (objects.size() == 0) {
                                    Toast.makeText(getApplicationContext(), "No Records found", Toast.LENGTH_LONG).show();
                                } else {
                                    for(int i=0 ; i < objects.size() ; i++){
                                        ParseObject UserObject = objects.get(i);
                                        if(UserObject.get("regions") != null) {
                                            List<String> user_regions = (ArrayList<String>) UserObject.get("regions");
                                            for (int j = 0; j < user_regions.size(); j++) {
                                                for (int k = 0; k < strings.length; k++) {
                                                    if (strings[k].equals(user_regions.get(j))) {
                                                        addtolist(objects.get(i), k);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    setChild();
                                }

                            } catch (NullPointerException n) {
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }catch (NullPointerException n){

            }
        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void addtolist(ParseUser object,int k){
        setHeader(k);

        if(region != null) {
            if (strings[k].equals(region)) {
                avail = true;
            }
        }
            switch (k) {
                case 0:
                    r0.add(object);
                    break;
                case 1:
                    r1.add(object);
                    break;
                case 2:
                    r2.add(object);
                    break;
                case 3:
                    r3.add(object);
                    break;
                case 4:
                    r4.add(object);
                    break;
                case 5:
                    r5.add(object);
                    break;
                case 6:
                    r6.add(object);
                    break;
                case 7:
                    r7.add(object);
                    break;
                case 8:
                    r8.add(object);
                    break;
                case 9:
                    r9.add(object);
                    break;
                case 10:
                    r10.add(object);
                    break;
                case 11:
                    r11.add(object);
                    break;
                case 12:
                    r12.add(object);
                    break;
                case 13:
                    r13.add(object);
                    break;
                case 14:
                    r14.add(object);
                    break;
                case 15:
                    r15.add(object);
                    break;
                case 16:
                    r16.add(object);
                    break;
                case 17:
                    r17.add(object);
                    break;
                default:
                    break;
            }
    }

    public void setHeader(int k){
        String HeaderItem = strings[k].toUpperCase();
        Boolean find = false;
        if(listDataHeader.size() == 0){
            listDataHeader.add(HeaderItem);
            find = true;
        }else {
            for (int i = 0; i < listDataHeader.size(); i++) {
                if (HeaderItem.equals(listDataHeader.get(i))) {
                    find = true;
                    break;
                }
            }
        }
        if(!find){
            listDataHeader.add(HeaderItem);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
