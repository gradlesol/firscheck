package com.gradlesol.firstcheck.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.List;

public class detailsAdapter extends ArrayAdapter<ParseObject> {
    protected Context mContext;
    protected List<ParseObject> mRecord;

    public detailsAdapter(Context context, List<ParseObject> record){
        super(context, R.layout.my_records_custom_layout, record);
        mContext = context;
        mRecord = record;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;
        //if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.my_records_custom_layout,null);
            holder = new ViewHolder();
            holder.ImageView_custom_molephoto = (CircularImageView)convertView.findViewById(R.id.ImageView_custom_molephoto);
            holder.ImageView_custom_photo1 = (CircularImageView)convertView.findViewById(R.id.ImageView_custom_photo1);
            holder.TextView_custom_date = (TextView)convertView.findViewById(R.id.TextView_custom_date);
            holder.TextView_custom_label = (TextView)convertView.findViewById(R.id.TextView_custom_label);
            holder.TextView_custom_code = (TextView)convertView.findViewById(R.id.TextView_custom_code);

            convertView.setTag(holder);
        //} else {
        //    holder = (ViewHolder)convertView.getTag();
        //}

        ParseObject recordObject = mRecord.get(position);
        try {

            ParseFile fileObject = recordObject.getParseFile("molephoto");
            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        if(data != null)
                        holder.ImageView_custom_molephoto.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });


            fileObject = recordObject.getParseFile("photo1");
            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        if(data != null)
                        holder.ImageView_custom_photo1.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });
            holder.TextView_custom_date.setText(recordObject.getCreatedAt().toString());
            holder.TextView_custom_label.setText(recordObject.getString("label"));
            holder.TextView_custom_code.setText(recordObject.getString("code"));
        }catch (NullPointerException e){

        }
        return convertView;
    }

    public static class ViewHolder{
        CircularImageView ImageView_custom_molephoto,ImageView_custom_photo1;
        TextView TextView_custom_date,TextView_custom_label,TextView_custom_code;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }
}
