package com.gradlesol.firstcheck.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.constants;

/**
 * Created by Gradle on 06-Aug-16.
 */
public class regionAdapter extends ArrayAdapter<String> {

    protected Context mContext;
    protected String[] mStrings;
    private LayoutInflater inflater;
    private SparseBooleanArray mSelectedItemsIds;

    public regionAdapter(Context context, String[] strings){
        super(context, R.layout.my_records_custom_layout, strings);
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        mStrings = strings;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;
        //if(convertView == null){
        convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_select_region,null);
        holder = new ViewHolder();
        holder.ImageView_custom_select = (ImageView)convertView.findViewById(R.id.ImageView_custom_select);
        holder.TextView_custom_select = (TextView)convertView.findViewById(R.id.TextView_custom_select);
        holder.LinearLayout_custom_select = (LinearLayout)convertView.findViewById(R.id.LinearLayout_custom_select);

        convertView.setTag(holder);
        //} else {
        //    holder = (ViewHolder)convertView.getTag();
        //}

        final String list = mStrings[position];
        holder.TextView_custom_select.setText(list);
        holder.TextView_custom_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.ImageView_custom_select.getVisibility() == View.INVISIBLE) {
                    holder.ImageView_custom_select.setVisibility(View.VISIBLE);
                    holder.LinearLayout_custom_select.setBackgroundColor(Color.parseColor("#cecdcd"));
                    constants.region.add(list);
                }else {
                    holder.ImageView_custom_select.setVisibility(View.INVISIBLE);
                    holder.LinearLayout_custom_select.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    constants.region.remove(list);
                }
            }
        });
        return convertView;
    }

    public static class ViewHolder{
        ImageView ImageView_custom_select;
        TextView TextView_custom_select;
        LinearLayout LinearLayout_custom_select;

    }

    public void toggleSelection(int position) {
        //selectView(position, !mSelectedItemsIds.get(position));
    }


    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

}
