package com.gradlesol.firstcheck.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.dermatolologistActivity;
import com.gradlesol.firstcheck.my_recordsActivity;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.List;

public class details_savedAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<ParseObject> mRecord;

    public details_savedAdapter(Context context, List<ParseObject> record){
        //super(context, R.layout.my_records_custom_layout_saved, record);
        mContext = context;
        mRecord = record;
    }

    @Override
    public int getCount() {
        return mRecord.size();
    }

    @Override
    public Object getItem(int position) {
        return mRecord.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;
        //if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.my_records_custom_layout_saved,null);
            holder = new ViewHolder();
            holder.ImageView_custom_molephoto = (CircularImageView)convertView.findViewById(R.id.ImageView_custom_molephoto);
            holder.ImageView_custom_photo1 = (CircularImageView)convertView.findViewById(R.id.ImageView_custom_photo1);
            holder.TextView_custom_date = (TextView)convertView.findViewById(R.id.TextView_custom_date);
            holder.TextView_custom_label = (TextView)convertView.findViewById(R.id.TextView_custom_label);
            holder.TextView_custom_code = (TextView)convertView.findViewById(R.id.TextView_custom_code);
            holder.Button_custom_export = (Button)convertView.findViewById(R.id.Button_custom_export);
            holder.ImageView_custom_que = (ImageView)convertView.findViewById(R.id.ImageView_custom_que);

        holder.ImageView_custom_que.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                View promptView = layoutInflater.inflate(R.layout.prompt1, null);
                final TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
                final TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
                final Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

                final Dialog dialog = new Dialog(mContext, R.style.CustomDialog);
                prompt1_title.setText("Review");
                prompt1_message.setText("\n" +
                        "If you have not previously sent this skin concern to a dermatologist for review (i.e. you have just saved it to your 'My Records' for future reference), then choose this option to submit to a dermatologist");
                prompt1_button.setText("OK");
                dialog.setContentView(promptView);
                dialog.setCancelable(false);
                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                prompt1_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });

            convertView.setTag(holder);
        //} else {
        //    holder = (ViewHolder)convertView.getTag();
        //}

        final ParseObject recordObject = mRecord.get(position);
        try {

            ParseFile fileObject = recordObject.getParseFile("molephoto");
            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        if(data != null)
                        holder.ImageView_custom_molephoto.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });


            fileObject = recordObject.getParseFile("photo1");
            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {

                    if (e == null) {
                        if(data != null)
                        holder.ImageView_custom_photo1.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });
            holder.TextView_custom_date.setText(recordObject.getCreatedAt().toString());
            holder.TextView_custom_label.setText(recordObject.getString("label"));
            holder.TextView_custom_code.setText(recordObject.getString("code"));

            holder.Button_custom_export.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(
                            new Intent(mContext.getApplicationContext(),dermatolologistActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .putExtra("region",recordObject.getString("region"))
                                    .putExtra("caseNo",recordObject.getString("caseNo"))
                                    .putExtra("myrecords",true)
                    );
                }
            });

        }catch (NullPointerException e){

        }
        return convertView;
    }

    public static class ViewHolder{
        CircularImageView ImageView_custom_molephoto,ImageView_custom_photo1;
        Button Button_custom_export;
        TextView TextView_custom_date,TextView_custom_label,TextView_custom_code;
        ImageView ImageView_custom_que;

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }
}
