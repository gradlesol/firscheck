package com.gradlesol.firstcheck.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

public class dermatologistAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<ParseUser> mRecord;

    public dermatologistAdapter(Context context, List<ParseUser> record){
        //super(context, R.layout.dermatologist_custom_layout, record);
        mContext = context;
        mRecord = record;
    }

    @Override
    public int getCount() {
        return mRecord.size();
    }

    @Override
    public Object getItem(int position) {
        return mRecord.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final ViewHolder holder;
            convertView = LayoutInflater.from(mContext).inflate(R.layout.dermatologist_custom_layout,null);
            holder = new ViewHolder();
            holder.ImageView_dermcustom_photo = (CircularImageView)convertView.findViewById(R.id.ImageView_dermcustom_photo);
            holder.TextView_dermcustom_name = (TextView)convertView.findViewById(R.id.TextView_dermcustom_name);
            holder.TextView_dermcustom_qualification = (TextView)convertView.findViewById(R.id.TextView_dermcustom_qualification);
            holder.TextView_dermcustom_cname = (TextView)convertView.findViewById(R.id.TextView_dermcustom_cname);
            holder.TextView_dermcustom_clocation = (TextView)convertView.findViewById(R.id.TextView_dermcustom_clocation);
            convertView.setTag(holder);

            ParseObject recordObject = mRecord.get(position);
            try {
                ParseFile fileObject = recordObject.getParseFile("Photo");
                /*String imageUrl = fileObject.getUrl();
                Uri imageUri = Uri.parse(imageUrl);
                Picasso.with(mContext)
                        .load(imageUri.toString())
                        .into(holder.ImageView_dermcustom_photo);*/

            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        if(data != null)
                        holder.ImageView_dermcustom_photo.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });
            }catch (NullPointerException n){}
                holder.TextView_dermcustom_name.setText(recordObject.getString("Name"));
                holder.TextView_dermcustom_qualification.setText(recordObject.getString("Qualification"));
                holder.TextView_dermcustom_cname.setText(recordObject.getString("Clinic"));
                holder.TextView_dermcustom_clocation.setText(recordObject.getString("Location"));
        return convertView;
    }

    public static class ViewHolder{
        CircularImageView ImageView_dermcustom_photo;
        TextView TextView_dermcustom_name,
                TextView_dermcustom_qualification,
                TextView_dermcustom_cname,
                TextView_dermcustom_clocation;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }
}
