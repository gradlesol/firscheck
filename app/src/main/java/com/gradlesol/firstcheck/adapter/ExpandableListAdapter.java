package com.gradlesol.firstcheck.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Gradle on 08-Aug-16.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mlistDataHeader;
    private HashMap<String, List<ParseUser>> mlistDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<ParseUser>> listChildData) {
        this.mContext = context;
        this.mlistDataHeader = listDataHeader;
        this.mlistDataChild = listChildData;
    }

    @Override
    public int getGroupCount() {
        return this.mlistDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mlistDataChild.get(this.mlistDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mlistDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mlistDataChild.get(this.mlistDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        //if (convertView == null) {
        LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.custom_list_group, null);
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);
        //}
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        //final String childText = (String) getChild(groupPosition, childPosition);

        //if (convertView == null) {
        LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.dermatologist_custom_layout, null);
        //}

        final ParseObject recordObject = (ParseObject) getChild(groupPosition,childPosition);

        holder = new ViewHolder();
        holder.ImageView_dermcustom_photo = (CircularImageView)convertView.findViewById(R.id.ImageView_dermcustom_photo);
        holder.TextView_dermcustom_name = (TextView)convertView.findViewById(R.id.TextView_dermcustom_name);
        holder.TextView_dermcustom_qualification = (TextView)convertView.findViewById(R.id.TextView_dermcustom_qualification);
        holder.TextView_dermcustom_cname = (TextView)convertView.findViewById(R.id.TextView_dermcustom_cname);
        holder.TextView_dermcustom_clocation = (TextView)convertView.findViewById(R.id.TextView_dermcustom_clocation);
        convertView.setTag(holder);

        try {
            ParseFile fileObject = recordObject.getParseFile("Photo");
            fileObject.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    if (e == null) {
                        if(data != null)
                            holder.ImageView_dermcustom_photo.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                    }
                }
            });
            /*ParseFile fileObject = recordObject.getParseFile("Photo");
            String imageUrl = fileObject.getUrl();
            Uri imageUri = Uri.parse(imageUrl);
            Picasso.with(mContext)
                    .load(imageUri.toString())
                    .placeholder(R.drawable.icon_nav_dermatologist)
                    .error(R.drawable.icon_nav_dermatologist)
                    .into(holder.ImageView_dermcustom_photo);*/
        }catch (NullPointerException n){}
        holder.TextView_dermcustom_name.setText(recordObject.getString("Name"));
        holder.TextView_dermcustom_qualification.setText(recordObject.getString("Qualification"));
        holder.TextView_dermcustom_cname.setText(recordObject.getString("Clinic"));
        holder.TextView_dermcustom_clocation.setText(recordObject.getString("Location"));

        /*
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);
        */
        return convertView;
    }

    public static class ViewHolder{
        CircularImageView ImageView_dermcustom_photo;
        TextView TextView_dermcustom_name,
                TextView_dermcustom_qualification,
                TextView_dermcustom_cname,
                TextView_dermcustom_clocation;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
