package com.gradlesol.firstcheck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.adapter.regionAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class select_regionActivity extends AppCompatActivity {

    ListView listView;
    String[] strings = {
            "Northland Region",
            "Auckland Region",
            "Walkato Region",
            "Bay of Plenty Region",
            "Gisborne Region",
            "Hawke's Bay Region",
            "Taranaki Region",
            "Manawatu-Wanganui Region",
            "Wellington Region",
            "Tasman Region",
            "Nelson Region",
            "Marlborough Region",
            "West Coast Region",
            "Canterbury Region",
            "Otago Region",
            "Southland Region",
            "Chatham Islands Territory",
            "Area Outside Territorial Authority"
    };
    ArrayList<String> region = new ArrayList<String >();

    Button Button_select_region;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_region);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Arrays.sort(strings);

        if(constants.region == null){

        }else {
            if(constants.region.size() != 0)
                constants.region.clear();
        }

        listView = (ListView)findViewById(R.id.listview_select_region);
        final regionAdapter adapter = new regionAdapter(getApplicationContext(), strings);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setItemsCanFocus(false);
        listView.setAdapter(adapter);
        /*
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
                listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                    @Override
                    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                        adapter.toggleSelection(position);
                    }

                    @Override
                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        return false;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        return false;
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode mode) {

                    }
                });
                return false;
            }
        });
*/
        Button_select_region = (Button)findViewById(R.id.Button_select_region);
        Button_select_region.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(constants.region.size() == 0){
                    Toast.makeText(getApplicationContext(),"You have not selected anything",Toast.LENGTH_SHORT).show();
                }else {
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
