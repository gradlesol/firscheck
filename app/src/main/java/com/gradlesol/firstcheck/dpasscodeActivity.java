package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.onesignal.OneSignal;

public class dpasscodeActivity extends AppCompatActivity {

    EditText EditText_userpasscode;
    TextView TextView_userpasscode_back,TextView_passcode_text;
    Boolean FirstRun;
    Button Button_passcode_save;

    String user,pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passcode);
        getSupportActionBar().hide();

        TextView_passcode_text = (TextView)findViewById(R.id.TextView_passcode_text);

        user = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginUsername", null);
        pwd = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginPassword", null);

        if(user == null || pwd == null){
            startActivity(new Intent(getApplicationContext(),login_dermatologistActivity.class));
            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putString("FirstRundPasscode", null)
                    .apply();
            finish();
        }else {

                ParseUser.logInInBackground(user, pwd, new LogInCallback() {
                    public void done(final ParseUser user, ParseException e) {
                        if (user != null) {
                            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                                @Override
                                public void idsAvailable(String userId, String registrationId) {
                                    Log.d("debug", "User:" + userId);
                                    if (registrationId != null)
                                        Log.d("debug", "registrationId:" + registrationId);
                                    user.put("uid",userId);
                                }
                            });
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e == null) {
                                        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                                .edit()
                                                .putString("LoginUsername", user.getUsername())
                                                .putString("LoginPassword",pwd)
                                                .apply();
                                    }
                                }
                            });
                        } else {
                            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                    .edit()
                                    .putString("LoginUsername", null)
                                    .putString("LoginPassword",null)
                                    .apply();
                        }
                        if (e != null) {
                            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(),login_dermatologistActivity.class));
                            finish();
                        }
                    }
                });
        }

        checkFirstRunPasscode();

        Button_passcode_save = (Button)findViewById(R.id.Button_passcode_save);
        Button_passcode_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEmpty(EditText_userpasscode)){

                    Toast.makeText(getApplicationContext(),"Empty",Toast.LENGTH_LONG).show();
                } else {
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .edit()
                            .putString("FirstRundPasscode", EditText_userpasscode.getText().toString())
                            .apply();
                    startActivity(new Intent(getApplicationContext(),your_casesActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            }
        });

        EditText_userpasscode = (EditText)findViewById(R.id.EditText_userpasscode);
        EditText_userpasscode.setHint("Dermatologist Passcode");

        TextView_userpasscode_back = (TextView)findViewById(R.id.TextView_userpasscode_back);
        TextView_userpasscode_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        if(FirstRun){
            TextView_passcode_text.setText("Create your 4 digit passcode.");
            Button_passcode_save.setVisibility(View.VISIBLE);
        }else{
            TextView_passcode_text.setText("Enter your 4 digit passcode to continue.");
            Button_passcode_save.setVisibility(View.INVISIBLE);
            final String password = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("FirstRundPasscode", null);
            EditText_userpasscode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (EditText_userpasscode.getText().toString().equals(password)) {

                        startActivity(new Intent(getApplicationContext(), your_casesActivity.class));
                        finish();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }


    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    public void checkFirstRunPasscode() {
        String password= getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("FirstRundPasscode", null);
        if (password == null){
            FirstRun = true;
        } else {
            FirstRun = false;
        }
    }

}
