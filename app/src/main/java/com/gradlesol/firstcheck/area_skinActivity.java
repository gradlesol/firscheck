package com.gradlesol.firstcheck;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.ByteArrayOutputStream;

public class area_skinActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_area_front,Button_area_back,Button_area_continue;
    TouchImageView ImageView_area_body;
    String flag = "front";

    private DbHelper dbHelper;

    String caseNo;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_skin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DbHelper(getApplicationContext());

        Button_area_front = (Button)findViewById(R.id.Button_area_front);
        Button_area_front.setOnClickListener(this);
        Button_area_back = (Button)findViewById(R.id.Button_area_back);
        Button_area_back.setOnClickListener(this);
        Button_area_continue = (Button)findViewById(R.id.Button_area_continue);
        Button_area_continue.setOnClickListener(this);

        ImageView_area_body = (TouchImageView) findViewById(R.id.ImageView_area_body);
        ImageView_area_body.setImageResource(R.drawable.front);

        ImageView_area_body.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(flag.equals("front"))
                            ImageView_area_body.setImageResource(R.drawable.front);
                        else
                            ImageView_area_body.setImageResource(R.drawable.back);

                        Matrix inverse = new Matrix();
                        ImageView_area_body.getImageMatrix().invert(inverse);
                        float[] touchPoint = new float[] {event.getX(), event.getY()};
                        inverse.mapPoints(touchPoint);

                        touchPoint[0] = touchPoint[0] - 24;
                        touchPoint[1] = touchPoint[1] - 24;

                        Bitmap bitmap = ((BitmapDrawable)ImageView_area_body.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);

                        if (touchPoint[1]>=0 && touchPoint[0]>=0 && touchPoint[0] < bitmap.getWidth())
                        {
                        int pixel = bitmap.getPixel((int) touchPoint[0],(int) touchPoint[1]);
                        if(pixel != Color.parseColor("#ffffff")){
                            Bitmap bitmapCircle = BitmapFactory.decodeResource(getResources(), R.drawable.crossp);
                            bitmapCircle = Bitmap.createScaledBitmap(bitmapCircle, 48, 48, false);
                            ImageView_area_body.setImageBitmap(bitmap);
                            Canvas canvas = new Canvas(bitmap);
                            canvas.drawBitmap(bitmapCircle,touchPoint[0],touchPoint[1],null);
                            ImageView_area_body.setImageBitmap(bitmap);
                        }
                        }
                        ImageView_area_body.invalidate();
                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Button_area_front:
                ImageView_area_body.setImageResource(R.drawable.front);
                flag = "front";
                break;
            case R.id.Button_area_back:
                ImageView_area_body.setImageResource(R.drawable.back);
                flag = "back";
                break;
            case R.id.Button_area_continue :

                Bitmap bitmap = ((BitmapDrawable)ImageView_area_body.getDrawable()).getBitmap();
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] image=stream.toByteArray();

                dbHelper.updateRecord("molephoto",image);
                startActivity(new Intent(getApplicationContext(),detailsActivity.class));
            default:
                break;
        }
    }
}
