package com.gradlesol.firstcheck;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

public class takephotoActivity extends AppCompatActivity implements View.OnClickListener {

    private Camera mCamera;
    private CameraPreview mCameraPreview;

    TextView retake,use_photo,cancel;
    Button captureButton;
    ImageView ImageView_takephoto_rotate,ImageView_takephoto_flash;

    private DbHelper dbHelper;

    byte[] image;
    String type;
    String caseNo;
    Boolean flash = true,review = false;
    Camera.Parameters params;
    Boolean dermoscope=false,sequence = false,upload = false;
    int cameraId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_takephoto);
        getSupportActionBar().hide();
        type = getIntent().getStringExtra("type");
       // caseNo = getIntent().getStringExtra("caseNo");
        sequence = getIntent().getBooleanExtra("sequence",false);
        review = getIntent().getBooleanExtra("review",false);

        dbHelper = new DbHelper(getApplicationContext());

        /*
        if (mCamera == null){
            initCamera(0);
        }*/

        ImageView_takephoto_flash = (ImageView)findViewById(R.id.ImageView_takephoto_flash);
        mCamera = getCameraInstance();
        params = mCamera.getParameters();
        if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
            if(type.equals("dermoscope")){
                ImageView_takephoto_flash.setImageResource(R.drawable.flashoff);
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }else {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            }
            if(type.equals("second")){
                //params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            mCamera.setParameters(params);
        }else {
            ImageView_takephoto_flash.setVisibility(View.INVISIBLE);
        }
        mCameraPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mCameraPreview);

        retake = (TextView)findViewById(R.id.retake);
        retake.setOnClickListener(this);

        use_photo = (TextView)findViewById(R.id.use_photo);
        use_photo.setOnClickListener(this);

        cancel = (TextView)findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        ImageView_takephoto_rotate = (ImageView)findViewById(R.id.ImageView_takephoto_rotate);
        ImageView_takephoto_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateCamera();
            }
        });

        ImageView_takephoto_flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flash){
                    flash = false;
                    ImageView_takephoto_flash.setImageResource(R.drawable.flashoff);
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    mCamera.setParameters(params);
                    mCamera.startPreview();
                }else {
                    flash = true;
                    ImageView_takephoto_flash.setImageResource(R.drawable.flash);
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    mCamera.setParameters(params);
                    mCamera.startPreview();
                }
            }
        });

        captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captureButton.setVisibility(View.INVISIBLE);
                cancel.setVisibility(View.INVISIBLE);
                retake.setVisibility(View.VISIBLE);
                use_photo.setVisibility(View.VISIBLE);
                mCamera.takePicture(null, null, mPicture);
            }
        });

        showDialog();
    }

    public void rotateCamera() {

        if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
            cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        else{
            cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        initCamera(cameraId);
    }

    public void initCamera(int potato){

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.removeView(mCameraPreview);
        mCamera = Camera.open(potato);
        if(potato == 0){
            params = mCamera.getParameters();
            if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
                ImageView_takephoto_flash.setVisibility(View.VISIBLE);
                if(flash) {
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                }
                mCamera.setParameters(params);
            }else {
                ImageView_takephoto_flash.setVisibility(View.INVISIBLE);
            }
        }else {
            ImageView_takephoto_flash.setVisibility(View.INVISIBLE);
        }

        if (mCamera != null) {
            mCameraPreview = new CameraPreview(this, mCamera);;//create a SurfaceView to show camera data
            preview.addView(mCameraPreview);//add the SurfaceView to the layout
        }
        //mCamera.startPreview();
    }

    public void showDialog(){
        if(type.equals("first")){

            String Title = "First photo - 20cm distance";
            String Message = "\n\n\nTake a photo using your device from a distance of approximately twenty centimetres (20cm) from the area of " +
                    "skin concern.\n\nFor best results, take the photo in good light and ensure the photo is in focus.\n\n\n";

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompt2, null);
            TextView prompt2_title = (TextView) promptView.findViewById(R.id.prompt2_title);
            TextView prompt2_message = (TextView) promptView.findViewById(R.id.prompt2_message);
            Button prompt2_button = (Button)promptView.findViewById(R.id.prompt2_button);

            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
            builder.setView(promptView);
            builder.setCancelable(false);
            prompt2_title.setText(Title);
            prompt2_message.setText(Message);
            prompt2_button.setText("Continue");
            final AlertDialog alert = builder.create();
            prompt2_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });
            alert.show();
        }else if(type.equals("second")){

            String Title = "Second photo - 10cm distance";
            String Message = "\n\nTake a photo using your device from a distance of approximately ten centimetres (10cm) from the area of " +
                    "skin concern.\n\nFor best results, take the photo in good light and ensure the photo is in focus.\n" +
                    "\n";

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompt3, null);
            TextView prompt3_title = (TextView) promptView.findViewById(R.id.prompt3_title);
            TextView prompt3_message = (TextView) promptView.findViewById(R.id.prompt3_message);
            Button prompt3_button = (Button)promptView.findViewById(R.id.prompt3_button);

            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
            builder.setView(promptView);
            builder.setCancelable(false);
            prompt3_title.setText(Title);
            prompt3_message.setText(Message);
            prompt3_button.setText("Continue");
            final AlertDialog alert = builder.create();
            prompt3_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });
            alert.show();

        }else if(type.equals("dermoscope")) {


            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompt4, null);
            TextView prompt4_title = (TextView) promptView.findViewById(R.id.prompt4_title);
            TextView prompt4_message = (TextView) promptView.findViewById(R.id.prompt4_message);
            Button prompt4_button = (Button)promptView.findViewById(R.id.prompt4_button);
            Button prompt4_button_skip = (Button)promptView.findViewById(R.id.prompt4_button_skip);

            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
            builder.setView(promptView);
            builder.setCancelable(false);
            prompt4_title.setText("Third Photo - SkinScope");
            prompt4_message.setText("\n\nTake a photo of the same area of skin concern after attaching the Firstcheck SkinScope to your device.\n\nPlease ensure the photo is in focus.\n\n");
            prompt4_button.setText("Continue");
            final AlertDialog alert = builder.create();
            prompt4_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });
            prompt4_button_skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), review_photosActivity.class));
                    finish();
                }
            });
            alert.show();
        }
    }


    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open(0);
        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            image = data;
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.use_photo:
                setPhoto();
                break;
            case R.id.retake:
                fretake();
                break;
            case R.id.cancel:
                if(review){
                    startActivity(new Intent(getApplicationContext(),review_photosActivity.class));
                }else {

                }
                finish();
                break;
            default:
                break;
        }
    }

    public void fretake(){
        mCamera.startPreview();
        retake.setVisibility(View.INVISIBLE);
        use_photo.setVisibility(View.INVISIBLE);
        cancel.setVisibility(View.VISIBLE);
        captureButton.setVisibility(View.VISIBLE);
    }

    public void setPhoto(){
        if(sequence){
            if(type.equals("first")){
                uploadPhoto("photo1");
                type = "second";
                showDialog();
                fretake();
            }else if(type.equals("second")){
                sequence = true;
                uploadPhoto("photo2");
                startActivity(new Intent(getApplicationContext(),dermatodailActivity.class));
                finish();
            }
        }else {
            if(type.equals("first")){
                uploadPhoto("photo1");
            }else if(type.equals("second")){
                uploadPhoto("photo2");
            }else if(type.equals("dermoscope")){
                uploadPhoto("photo3");
            }
            if(upload) {
                startActivity(new Intent(getApplicationContext(), review_photosActivity.class));
                sequence = false;
                finish();
            }
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);


        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }

    public void uploadPhoto(final String ptype){
        if(image != null) {

            Bitmap bmp = decodeSampledBitmapFromResource(image, 100, 100);
            //Bitmap bmp = BitmapFactory.decodeByteArray(image,0,image.length);
            bmp = rotateImage(90,bmp);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            image = stream.toByteArray();
            dbHelper.updateRecord(ptype, image);
            upload = true;
        }


    }

    public Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
    }
}
