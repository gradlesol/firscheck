package com.gradlesol.firstcheck;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class review_photosActivity extends AppCompatActivity implements View.OnClickListener {


    com.mikhaellopez.circularimageview.CircularImageView ImageView_first,ImageView_second,ImageView_dermoscope;
    TextView TextView_first,TextView_second,TextView_dermoscope;
    Button Button_review_continue;

    private DbHelper dbHelper;
    private Cursor cursor;

    String type;
    String caseNo;
    byte[] photo1,photo2,photo3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_photos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        dbHelper = new DbHelper(getApplicationContext());

        cursor = dbHelper.selectRecords();


        if (cursor.moveToFirst()) {
            do {
                photo1 = cursor.getBlob(cursor.getColumnIndex("photo1"));
                photo2 = cursor.getBlob(cursor.getColumnIndex("photo2"));
                photo3 = cursor.getBlob(cursor.getColumnIndex("photo3"));
            } while (cursor.moveToNext());
        }
        cursor.close();

        ImageView_first = (com.mikhaellopez.circularimageview.CircularImageView)findViewById(R.id.ImageView_first);
        Bitmap bmp = decodeSampledBitmapFromResource(photo1, 100, 100);
        ImageView_first.setImageBitmap(bmp);

        ImageView_second = (com.mikhaellopez.circularimageview.CircularImageView)findViewById(R.id.ImageView_second);
        bmp = decodeSampledBitmapFromResource(photo2, 100, 100);
        ImageView_second.setImageBitmap(bmp);

        ImageView_dermoscope = (com.mikhaellopez.circularimageview.CircularImageView)findViewById(R.id.ImageView_dermoscope);
        if(photo3 != null) {
            bmp = decodeSampledBitmapFromResource(photo3, 100, 100);
            ImageView_dermoscope.setImageBitmap(bmp);
        }


        TextView_first = (TextView)findViewById(R.id.TextView_retake_first);
        TextView_first.setOnClickListener(this);
        TextView_second = (TextView)findViewById(R.id.TextView_retake_second);
        TextView_second.setOnClickListener(this);
        TextView_dermoscope = (TextView)findViewById(R.id.TextView_retake_dermoscope);
        TextView_dermoscope.setOnClickListener(this);

        Button_review_continue = (Button)findViewById(R.id.Button_review_continue);
        Button_review_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),area_skinActivity.class));
            }
        });
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.TextView_retake_first :
                startActivity(new Intent(getApplicationContext(),takephotoActivity.class)
                        .putExtra("type","first")
                        .putExtra("review",true)
                        .putExtra("sequence",false));
                finish();
                break;

            case R.id.TextView_retake_second :
                startActivity(new Intent(getApplicationContext(),takephotoActivity.class)
                        .putExtra("type","second")
                        .putExtra("review",true)
                        .putExtra("sequence",false));
                finish();
                break;

            case R.id.TextView_retake_dermoscope :
                startActivity(new Intent(getApplicationContext(),takephotoActivity.class)
                        .putExtra("type","dermoscope")
                        .putExtra("review",true)
                        .putExtra("sequence",false));
                finish();
                break;
            default:
                break;
        }

    }

}
