package com.gradlesol.firstcheck;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.Random;

public class detailsActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_details_continue;
    Button Button_details_exit;

    EditText
            EditText_details_description,
            EditText_details_code,
            EditText_details_label;
    ImageView ImageView_details_description, ImageView_details_label;

    TextView TextView_details_wht,
            EditText_details_region,
            EditText_details_ethnicity,
            EditText_details_gender,
            EditText_details_age,
            EditText_details_howlong,
            EditText_details_insurance;

    String caseNo;

    String android_id;

    String type,region;
    byte[] photo1, photo2, photo3, molephoto;

    private DbHelper dbHelper;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DbHelper(getApplicationContext());
        cursor = dbHelper.selectRecords();

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        if (cursor.moveToFirst()) {
            do {
                type = cursor.getString(cursor.getColumnIndex("type"));
                photo1 = cursor.getBlob(cursor.getColumnIndex("photo1"));
                photo2 = cursor.getBlob(cursor.getColumnIndex("photo2"));
                photo3 = cursor.getBlob(cursor.getColumnIndex("photo3"));
                molephoto = cursor.getBlob(cursor.getColumnIndex("molephoto"));
            } while (cursor.moveToNext());
        }
        cursor.close();

        getRandomCaseID();

        Button_details_continue = (Button) findViewById(R.id.Button_details_continue);
        Button_details_continue.setOnClickListener(this);

        Button_details_exit = (Button) findViewById(R.id.Button_details_exit);
        Button_details_exit.setOnClickListener(this);

        EditText_details_gender = (TextView) findViewById(R.id.EditText_details_gender);
        EditText_details_gender.setOnClickListener(this);
        EditText_details_age = (TextView) findViewById(R.id.EditText_details_age);
        EditText_details_age.setOnClickListener(this);
        EditText_details_howlong = (TextView) findViewById(R.id.EditText_details_howlong);
        EditText_details_howlong.setOnClickListener(this);
        EditText_details_description = (EditText) findViewById(R.id.EditText_details_description);
        EditText_details_region = (TextView) findViewById(R.id.EditText_details_region);
        EditText_details_region.setOnClickListener(this);
        EditText_details_ethnicity = (TextView) findViewById(R.id.EditText_details_ethnicity);
        EditText_details_ethnicity.setOnClickListener(this);
        EditText_details_code = (EditText) findViewById(R.id.EditText_details_code);
        EditText_details_label = (EditText) findViewById(R.id.EditText_details_label);
        EditText_details_insurance = (TextView) findViewById(R.id.EditText_details_insurance);
        EditText_details_insurance.setOnClickListener(this);

        TextView_details_wht = (TextView) findViewById(R.id.EditText_details_wht);
        TextView_details_wht.setOnClickListener(this);

        ImageView_details_description = (ImageView) findViewById(R.id.ImageView_details_description);
        ImageView_details_description.setOnClickListener(this);

        ImageView_details_label = (ImageView) findViewById(R.id.ImageVIew_details_label);
        ImageView_details_label.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        if (id == R.id.Button_details_continue || id == R.id.Button_details_exit) {
            switch (id) {
                case R.id.Button_details_continue:
                    if(isNetworkAvailable()){
                        dbHelper.updateData("caseNo",caseNo);
                        dbHelper.updateData("gender", EditText_details_gender.getText().toString());
                        dbHelper.updateData("age", EditText_details_age.getText().toString());
                        dbHelper.updateData("howlong", EditText_details_howlong.getText().toString());
                        dbHelper.updateData("description", EditText_details_description.getText().toString());
                        dbHelper.updateData("region", EditText_details_region.getText().toString());
                        region = EditText_details_region.getText().toString();
                        dbHelper.updateData("ethnicity", EditText_details_ethnicity.getText().toString());
                        dbHelper.updateData("code", EditText_details_code.getText().toString());
                        dbHelper.updateData("label", EditText_details_label.getText().toString());
                        dbHelper.updateData("insurance", EditText_details_insurance.getText().toString());
                        startActivity(new Intent(getApplicationContext(), dermatolologistActivity.class).putExtra("region",region));
                    }else {
                        Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.Button_details_exit:
                    dbHelper.updateData("caseNo",caseNo);
                    dbHelper.updateData("gender", EditText_details_gender.getText().toString());
                    dbHelper.updateData("age", EditText_details_age.getText().toString());
                    dbHelper.updateData("howlong", EditText_details_howlong.getText().toString());
                    dbHelper.updateData("description", EditText_details_description.getText().toString());
                    dbHelper.updateData("region", EditText_details_region.getText().toString());
                    dbHelper.updateData("ethnicity", EditText_details_ethnicity.getText().toString());
                    dbHelper.updateData("code", EditText_details_code.getText().toString());
                    dbHelper.updateData("label", EditText_details_label.getText().toString());
                    dbHelper.updateData("insurance", EditText_details_insurance.getText().toString());
                    startActivity(new Intent(getApplicationContext(),uploadingActivity.class).putExtra("saved",true));
                    /*if(isNetworkAvailable()){
                        ParseObject object = new ParseObject(constants.TABLE_NAME);
                        object.put("caseNo", caseNo);
                        object.put("type", type);
                        object.put("username",android_id);
                        object.put("gender", EditText_details_gender.getText().toString());
                        object.put("age", EditText_details_age.getText().toString());
                        object.put("howlong", EditText_details_howlong.getText().toString());
                        object.put("description", EditText_details_description.getText().toString());
                        object.put("region", EditText_details_region.getText().toString());
                        object.put("ethnicity", EditText_details_ethnicity.getText().toString());
                        object.put("code", EditText_details_code.getText().toString());
                        object.put("label", EditText_details_label.getText().toString());
                        object.put("insurance", EditText_details_insurance.getText().toString());
                            String partFilename = "photo1" + ".jpg";
                            if(photo1 != null) {
                                final ParseFile photoFile = new ParseFile(partFilename, photo1);
                                object.put("photo1", photoFile);
                            }
                            if(photo2 != null) {
                                partFilename ="photo2" + ".jpg";
                                final ParseFile photoFile = new ParseFile(partFilename, photo2);
                                object.put("photo2", photoFile);
                            }
                            if(photo3 != null) {
                                partFilename ="photo3" + ".jpg";
                                final ParseFile photoFile = new ParseFile(partFilename, photo3);
                                object.put("photo3", photoFile);
                            }
                            if(molephoto != null) {
                                partFilename ="molephoto" + ".jpg";
                                final ParseFile photoFile = new ParseFile(partFilename, molephoto);
                                object.put("molephoto", photoFile);
                            }
                            object.put("saved", true);
                            object.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                        finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error: " + e.toString(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                    }else {
                        Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                    }*/
                    break;
                default:
                    break;
            }
        } else {
            switch (id){
                case R.id.EditText_details_gender :
                    final CharSequence item1[] = new CharSequence[] {"Male", "Female", "Gender Diverse"};
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                    builder1.setTitle("Select Gender:");
                    builder1.setItems(item1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_gender.setText(item1[which].toString());
                        }
                    });
                    builder1.show();
                    break;
                case R.id.EditText_details_age :
                    final CharSequence item2[] = new CharSequence[] {"16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99"};
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle("Select Age:");
                    builder2.setItems(item2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_age.setText(item2[which].toString());
                        }
                    });
                    builder2.show();
                    break;
                case R.id.EditText_details_howlong :
                    final CharSequence item3[] = new CharSequence[] {"Days", "Weeks", "Months", "Years","It Comes and Goes"};
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                    builder3.setTitle("Select How Long:");
                    builder3.setItems(item3, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_howlong.setText(item3[which].toString());
                        }
                    });
                    builder3.show();
                    break;
                case R.id.EditText_details_ethnicity :
                    final CharSequence item4[] = new CharSequence[] {"European", "Maori", "Pacific Peoples", "Asian","Middle East/Latin American/African","Other Ethnicity"};
                    AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                    builder4.setTitle("Select Ethnicity:");
                    builder4.setItems(item4, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_ethnicity.setText(item4[which].toString());
                        }
                    });
                    builder4.show();
                    break;
                case R.id.EditText_details_insurance :
                    final CharSequence item5[] = new CharSequence[] {"Southern Cross", "NIB", "AMP", "Other", "None"};
                    AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
                    builder5.setTitle("Select Gender:");
                    builder5.setItems(item5, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_insurance.setText(item5[which].toString());
                        }
                    });
                    builder5.show();
                    break;

                case R.id.EditText_details_region :
                    final CharSequence item6[] = new CharSequence[] {"Northland Region", "Auckland Region", "Walkato Region","Bay of Plenty Region","Gisborne Region","Hawke's Bay Region","Taranaki Region","Manawatu-Wanganui Region","Wellington Region","Tasman Region","Nelson Region","Marlborough Region","West Coast Region","Canterbury Region","Otago Region","Southland Region","Chatham Islands Territory","Area Outside Territorial Authority"};
                    AlertDialog.Builder builder6 = new AlertDialog.Builder(this);
                    builder6.setTitle("Select Region:");
                    builder6.setItems(item6, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText_details_region.setText(item6[which].toString());
                        }
                    });
                    builder6.show();
                    break;
                case R.id.ImageView_details_description :
                    show_dialog("Describe Skin Concern","\nE.g. Description/features. Any symptoms? Any aggravating/relieving features? Any changes? And if so, how & when? Any relevant medical/diagnostic information / current or previous treatment? Any family history? Any significant health problems / medication / allergies? Pregnant? For any rashes: (i)Does anyone you have had contact with have the same condition? (ii)Have you travelled outside New Zealand recently?","OK");
                    break;
                case R.id.ImageVIew_details_label :
                    show_dialog("Family Label","\nTo differentiate between any family members using the app on this device, add a first initial or nickname (remember to stay anonymous). Also add any other useful reference for later identifying this skin concern in your 'My Records'","OK");
                    break;
                case R.id.EditText_details_wht :
                    show_dialog("Save and Exit","\nYour consultation will not be sent to a dermatologist but will be saved in your 'My Records' (accessible from the 'Home' screen) enabling skin concern comparison over time.","OK");
                    break;
                default:
                    break;
            }
        }
    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    private static final String ALLOWED_CHARACTERS ="0123456789QWERTYUIOPASDFGHJKLZXCVBNM";

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    public void getRandomCaseID() {

        caseNo = getRandomString(6);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
        query.whereEqualTo("caseNo", caseNo);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object == null) {

                } else {
                    getRandomCaseID();
                }
            }
        });
    }
}
