package com.gradlesol.firstcheck;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class paymentActivity extends AppCompatActivity implements View.OnClickListener {


    String derm,name,dermuid,caseNo,from;

    Button Button_payment_paypal;
    SwitchCompat SwitchCompat_payment_accept;
    TextView TextView_payment_terms,TextView_payment_apply,TextView_payment_price;
    EditText EditText_payment_coupon;
    String price = "19.95";
    Boolean myrecords = false;

    private static final String TAG = "paymentExample";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    //sandbox//private static final String CONFIG_CLIENT_ID = "AY9MvS6CbBDAqtI9T8qHOZ85FqZrUt6o5XvZZGf4XfAAv74h5aD0EIwlGrJhskJVISmx8WJzMkUTMZ9N";
    private static final String CONFIG_CLIENT_ID = "Aft_6NzptYYtXC9o7M3ItDLo8MZAr9d2oWbdPLdah5xCm2-9GOML-pyemK8Bn_dsDV82eThP0LpQy5lf";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("firstcheck")
            .merchantPrivacyPolicyUri(Uri.parse("http://firstcheck.airsquare.com/privacy-policy.cfm"))
            .merchantUserAgreementUri(Uri.parse("http://firstcheck.airsquare.com/privacy-policy.cfm"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        derm = getIntent().getStringExtra("derm");
        name = getIntent().getStringExtra("name");
        dermuid = getIntent().getStringExtra("dermuid");
        myrecords = getIntent().getBooleanExtra("myrecords",false);
        if(myrecords){
            caseNo = getIntent().getStringExtra("caseNo");
        }

        EditText_payment_coupon = (EditText)findViewById(R.id.EditText_payment_coupon);

        TextView_payment_price = (TextView)findViewById(R.id.TextView_payment_price);

        TextView_payment_apply = (TextView)findViewById(R.id.TextView_payment_apply);
        TextView_payment_apply.setOnClickListener(this);

        Button_payment_paypal = (Button)findViewById(R.id.Button_payment_paypal);
        Button_payment_paypal.setOnClickListener(this);

        TextView_payment_terms = (TextView)findViewById(R.id.TextView_payment_terms);
        TextView_payment_terms.setOnClickListener(this);

        SwitchCompat_payment_accept = (SwitchCompat)findViewById(R.id.SwitchCompat_payment_accept);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Button_payment_paypal:

                if(SwitchCompat_payment_accept.isChecked()){
                    if(price.equals("0.00")){
                        if(myrecords){
                            startActivity(new Intent(getApplicationContext(),payment_successActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .putExtra("derm",derm)
                                    .putExtra("name",name)
                                    .putExtra("dermuid",dermuid)
                                    .putExtra("caseNo",caseNo)
                                    .putExtra("myrecords",myrecords)
                            );
                        }else {
                            startActivity(new Intent(getApplicationContext(),payment_successActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .putExtra("derm",derm)
                                    .putExtra("name",name)
                                    .putExtra("dermuid",dermuid)
                                    .putExtra("myrecords",myrecords)
                            );
                        }

                    }else {
                        onBuyPressed();
                    }
                }else {
                    show_dialog("Required","\nPlease scroll down to accept the Terms & Conditions in order to continue.","OK");
                }
                break;
            case R.id.TextView_payment_terms:

                startActivity(new Intent(getApplicationContext(),terms_displayActivity.class));

                break;
            case R.id.TextView_payment_apply :
                if((EditText_payment_coupon.getText().toString().equals("FCIAN00"))&&(derm.equals("ian@gmail.com"))){
                    price = "0.00";
                    TextView_payment_price.setText("$0.00");

                }else {
                    show_dialog("Coupon Error","\nYou have either entered an invalid coupon code or an invalid coupon code for the dermatologist you have selected.","OK");
                }
                break;
            default:
                break;
        }
    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(paymentActivity.this, PaymentActivity.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("19.95"), "USD","Consultation Fee",
                paymentIntent);
    }

    protected void displayResultText(String result) {
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        displayResultText("PaymentConfirmation info received from PayPal");
                        if(myrecords){
                            startActivity(new Intent(getApplicationContext(),payment_successActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .putExtra("derm",derm)
                                    .putExtra("name",name)
                                    .putExtra("dermuid",dermuid)
                                    .putExtra("caseNo",caseNo)
                                    .putExtra("myrecords",myrecords)
                            );
                        }else {
                            startActivity(new Intent(getApplicationContext(),payment_successActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .putExtra("derm",derm)
                                    .putExtra("name",name)
                                    .putExtra("dermuid",dermuid)
                                    .putExtra("myrecords",myrecords)
                            );
                        }
                        finish();

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");
                        startActivity(new Intent(getApplicationContext(),payment_successActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }
    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
