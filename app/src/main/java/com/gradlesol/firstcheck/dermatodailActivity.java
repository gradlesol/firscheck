package com.gradlesol.firstcheck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class dermatodailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dermatodail);
        getSupportActionBar().hide();

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);

        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(true);
        prompt1_title.setText("Firstcheck SkinScope");
        prompt1_message.setText("\nFor even better results, you may choose to take a highly-magnified photo of your area of skin concern using a Firstcheck SkinScope (ideal for moles and lesions). The Firstcheck SkinScope enables you to take high-quality 20x magnified dermoscopic photos, but it is not necessary for an initial assessment.\n\nProceed with your consultation to receive information about obtaining Firstcheck’s SkinScope for use in future consultations.");
        prompt1_button.setText("Okay");
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),takephotoActivity.class).putExtra("sequence",false).putExtra("type","dermoscope"));
                finish();
                alert.dismiss();
            }
        });
        alert.show();
    }
}
