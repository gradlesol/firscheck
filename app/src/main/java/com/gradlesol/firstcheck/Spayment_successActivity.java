package com.gradlesol.firstcheck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Spayment_successActivity extends AppCompatActivity {

    Button Button_spaymntsuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spayment_success);

        Button_spaymntsuccess = (Button)findViewById(R.id.Button_spaymntsuccess);
        Button_spaymntsuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
