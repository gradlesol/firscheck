package com.gradlesol.firstcheck;

import android.app.Application;
import android.content.Intent;

import com.onesignal.OneSignal;
import com.parse.Parse;
import com.parse.ParseACL;

import org.json.JSONObject;


public class Firstcheck extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new FirstcheckNotificationOpenedHandler())
                .init();

        OneSignal.setSubscription(true);
        OneSignal.enableInAppAlertNotification(true);
        OneSignal.enableNotificationsWhenActive(true);

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("9AYem5gtTWnJ4ZlG7RDyqrcc5MmHb9DbV7ZnyEFJ")
                .clientKey("Uezsb0iWVCQ7XPV4SrOYMQlFfWvg2Y2pWXJO9lDK")
                .server("http://firstcheck.herokuapp.com/parse/")
                .build());

        ParseACL defaultACL = new ParseACL();

        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }

    private class FirstcheckNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
            try {
                if (additionalData != null) {
                    if (additionalData.has("actionSelected"))
                        ;
                }
            } catch (Throwable t) {
            }

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        }

}
