package com.gradlesol.firstcheck;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.onesignal.OneSignal;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class case_detailsActivity extends AppCompatActivity implements View.OnClickListener {

    CircularImageView ImageView_case_details_molephoto,ImageView_case_details_photo1,ImageView_case_details_photo2,ImageView_case_details_photo3;
    TextView TextView_case_details_userData;
    TextView EditText_case_details_description;
    EditText EditText_case_details_response;
    Button Button_case_details_export;
    Button Button_case_details_send;

    byte[] molephoto,photo1,photo2,photo3;

    String caseNo,uid;
    String subject,email_msg;
    Boolean complete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        caseNo = getIntent().getStringExtra("caseNo");
        complete = getIntent().getBooleanExtra("completed",false);

        subject = "Case No:" + caseNo;

        /*final ProgressDialog dialog = new ProgressDialog(case_detailsActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();*/

        ImageView_case_details_molephoto = (CircularImageView)findViewById(R.id.ImageView_case_details_molephoto);
        ImageView_case_details_molephoto.setOnClickListener(this);

        ImageView_case_details_photo1 = (CircularImageView)findViewById(R.id.ImageView_case_details_photo1);
        ImageView_case_details_photo1.setOnClickListener(this);

        ImageView_case_details_photo2 = (CircularImageView)findViewById(R.id.ImageView_case_details_photo2);
        ImageView_case_details_photo2.setOnClickListener(this);

        ImageView_case_details_photo3 = (CircularImageView)findViewById(R.id.ImageView_case_details_photo3);
        ImageView_case_details_photo3.setOnClickListener(this);

        TextView_case_details_userData = (TextView)findViewById(R.id.TextView_case_details_userData);
        EditText_case_details_description = (TextView)findViewById(R.id.EditText_case_details_description);
        EditText_case_details_response = (EditText)findViewById(R.id.EditText_case_details_response);

        Button_case_details_export = (Button)findViewById(R.id.Button_case_details_export);
        Button_case_details_export.setOnClickListener(this);

        Button_case_details_send = (Button)findViewById(R.id.Button_case_detail_send);
        Button_case_details_send.setOnClickListener(this);

        try {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
            query.whereEqualTo("caseNo", caseNo);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (object == null) {

                    } else {
                        if (e == null) {
                            uid = object.getString("uid");
                            String userData = "Date: " + object.getCreatedAt().toString();
                            userData = userData + "\n\nType: " + object.getString("type");
                            userData = userData + "\n\nPatient's Label: " + object.getString("label");
                            userData = userData + "\n\nReference Code: " + object.getString("caseNo");
                            userData = userData + "\n\nGender: " + object.getString("gender");
                            userData = userData + "\n\nAge: " + object.getString("age");
                            userData = userData + "\n\nMedical Insurance: " + object.getString("insurance");
                            userData = userData + "\n\nHealth Practitioner Code: " + object.getString("code");
                            userData = userData + "\n\nEthnicity: " + object.getString("ethnicity");
                            userData = userData + "\n\nRegion: " + object.getString("region");
                            userData = userData + "\n\nHow long have you had it: "+ object.getString("howlong");
                            TextView_case_details_userData.setText(userData);

                            email_msg = userData + "\n\nYour Description:" + object.getString("description");
                            EditText_case_details_description.setText(object.getString("description"));

                            if(complete){
                                EditText_case_details_response.setText(object.getString("diagnosis"));
                                EditText_case_details_response.setEnabled(false);
                                Button_case_details_send.setVisibility(View.INVISIBLE);
                            }
                            if(object.getString("reviewedDate") != null)
                            email_msg = email_msg + "\n\nReview Date: "+object.getString("reviewedDate");
                            else email_msg = email_msg + "\n\nReview Date: ";
                            if(object.getString("diagnosis") != null)
                            email_msg = email_msg + "\n\nResponse: "+object.getString("diagnosis");
                            else email_msg = email_msg + "\n\nResponse: ";

                            //dialog.dismiss();
                            try {
                                ParseFile fileObject = object.getParseFile("molephoto");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            molephoto = data;
                                            ImageView_case_details_molephoto.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo1");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            photo1 = data;
                                            ImageView_case_details_photo1.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo2");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            photo2 = data;
                                            ImageView_case_details_photo2.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo3");
                                if(fileObject != null){
                                    ImageView_case_details_photo3.setVisibility(View.VISIBLE);
                                    fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            photo3 = data;
                                            ImageView_case_details_photo3.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });}
                            }catch (Exception e1){}
                        }
                    }
                }
            });
        }catch (NullPointerException n){}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.case_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;

            case R.id.action_Report :
                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View promptView = layoutInflater.inflate(R.layout.prompt5, null);
                TextView prompt5_title = (TextView) promptView.findViewById(R.id.prompt5_title);
                TextView prompt5_message = (TextView) promptView.findViewById(R.id.prompt5_message);
                Button prompt5_negative = (Button)promptView.findViewById(R.id.prompt5_negative);
                Button prompt5_positive = (Button)promptView.findViewById(R.id.prompt5_positive);

                AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
                builder.setView(promptView);
                builder.setCancelable(true);
                prompt5_title.setText("\nDo you want to report this case as inappropriate?\n");
                prompt5_message.setText("");
                prompt5_positive.setText("Report");
                prompt5_negative.setText("Cancel");
                final AlertDialog alert = builder.create();
                prompt5_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
                        query.whereEqualTo("caseNo", caseNo);
                        query.getFirstInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                if (object == null) {

                                } else {
                                    if (e == null) {
                                        if(object.get("Report") == null){
                                            object.put("Report",1);
                                        }else {
                                            object.put("Report", object.getInt("Report") + 1);
                                        }
                                        object.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if(e == null){
                                                    onBackPressed();
                                                    finish();
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
                prompt5_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();

                /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Do you want to report this case as inappropriate?")
                        .setCancelable(false)
                        .setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setPositiveButton("Report", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
                                query.whereEqualTo("caseNo", caseNo);
                                query.getFirstInBackground(new GetCallback<ParseObject>() {
                                    @Override
                                    public void done(ParseObject object, ParseException e) {
                                        if (object == null) {

                                        } else {
                                            if (e == null) {
                                                if(object.get("Report") == null){
                                                    object.put("Report",1);
                                                }else {
                                                    object.put("Report", object.getInt("Report") + 1);
                                                }
                                                object.saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        if(e == null){
                                                            onBackPressed();
                                                            finish();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImageView_case_details_molephoto:
                startActivity(new Intent(getApplicationContext(), photoActivity.class).putExtra("type", "molephoto").putExtra("caseNo", caseNo));
                break;
            case R.id.ImageView_case_details_photo1:
                startActivity(new Intent(getApplicationContext(), photoActivity.class).putExtra("type", "photo1").putExtra("caseNo", caseNo));
                break;
            case R.id.ImageView_case_details_photo2:
                startActivity(new Intent(getApplicationContext(), photoActivity.class).putExtra("type", "photo2").putExtra("caseNo", caseNo));
                break;
            case R.id.ImageView_case_details_photo3:
                startActivity(new Intent(getApplicationContext(), photoActivity.class).putExtra("type", "photo2").putExtra("caseNo", caseNo));
                break;
            case R.id.Button_case_details_export :
                Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
                email.setType("text/jpeg");
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, email_msg);
                ArrayList<Uri> uris = new ArrayList<Uri>();
                Uri u1 = Uri.fromFile(generatPhoto(molephoto,"molephoto"));
                uris.add(u1);
                Uri u2 = Uri.fromFile(generatPhoto(photo1,"photo1"));
                uris.add(u2);
                Uri u3 = Uri.fromFile(generatPhoto(photo2,"photo2"));
                uris.add(u3);
                email.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                break;
            case R.id.Button_case_detail_send :
                if(isEmpty(EditText_case_details_response)){
                    Toast.makeText(getApplicationContext(),"Response is empty.",Toast.LENGTH_LONG).show();
                } else {

                    ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                    postACL.setPublicReadAccess(true);
                    postACL.setPublicWriteAccess(true);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
                    query.whereEqualTo("caseNo", caseNo);
                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject obj, ParseException e) {
                            if (obj == null) {

                            } else {
                                if (e == null) {
                                    obj.put("reviewedDate",new Date());
                                    obj.put("diagnosis",EditText_case_details_response.getText().toString());
                                    obj.put("completed",true);
                                    obj.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if(e == null){
                                                try {
                                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'Your dermatologist has responded. Go to “My Records” to review'}, 'include_player_ids': ['" + uid + "']}"),
                                                            new OneSignal.PostNotificationResponseHandler() {
                                                                @Override
                                                                public void onSuccess(JSONObject response) {
                                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                                }
                                                                @Override
                                                                public void onFailure(JSONObject response) {
                                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                                }
                                                            });
                                                    startActivity(new Intent(getApplicationContext(),your_casesActivity.class).putExtra("completed",true));
                                                    finish();
                                                } catch (JSONException j) {
                                                }
                                            }else {
                                                Toast.makeText(getApplicationContext(),"IError :"+e.getMessage(),Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }else {
                                    Toast.makeText(getApplicationContext(),"OError :"+e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    public File generatPhoto(byte[] data, String type){

        File pictureFile = getOutputMediaFile(type);
        if (pictureFile == null) {
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {
        }
        return pictureFile;
    }
    private static File getOutputMediaFile(String type) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_"+ type+"_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }
}
