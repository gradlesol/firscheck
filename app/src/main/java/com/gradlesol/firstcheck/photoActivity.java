package com.gradlesol.firstcheck;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.gradlesol.firstcheck.imageutils.ImageLoader;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

public class photoActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ImageView_photo;
    Button Button_photo_prev,Button_photo_next;
    String current;
    Boolean avail = false;
    Bitmap bmp1,bmp2,bmp3,bmp4;
    //Uri photo1,photo2,photo3,molephoto;
    //private ImageLoader imgLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String type = getIntent().getStringExtra("type");
        current = type;
        String caseNo = getIntent().getStringExtra("caseNo");

        //imgLoader = new ImageLoader(this);

        ImageView_photo = (ImageView)findViewById(R.id.ImageView_photo);
        Button_photo_next = (Button)findViewById(R.id.Button_photo_next);
        Button_photo_next.setOnClickListener(this);
        Button_photo_prev = (Button)findViewById(R.id.Button_photo_prev);
        Button_photo_prev.setOnClickListener(this);

        if(type.equals("molephoto")){
            Button_photo_prev.setVisibility(View.INVISIBLE);
        }else if (type.equals("photo3")){
            Button_photo_next.setVisibility(View.INVISIBLE);
        }

        try {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
            query.whereEqualTo("caseNo", caseNo);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (object == null) {

                    } else {
                        if (e == null) {
                            try {
                                ParseFile fileObject = object.getParseFile(type);
                                //String imageUrl = fileObject.getUrl();
                                //Uri imageUri = Uri.parse(imageUrl);
                                //imgLoader.DisplayImage(imageUri.toString(),ImageView_photo);
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            ImageView_photo.setImageBitmap(decodeSampledBitmapFromResource(data, 500, 500));
                                        }
                                    }
                                });
                            }catch (NullPointerException n){}

                            try {
                                ParseFile fileObject = object.getParseFile("photo3");
                                //String imageUrl = fileObject.getUrl();
                                //photo3 = Uri.parse(imageUrl);
                                //avail = true;
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            bmp4 = decodeSampledBitmapFromResource(data, 500, 500);
                                            avail = true;
                                        }
                                    }
                                });

                            }catch (NullPointerException n){
                                avail = false;
                                if(type.equals("photo2")) {
                                    Button_photo_next.setVisibility(View.INVISIBLE);
                                }
                            }

                            try {
                                ParseFile fileObject = object.getParseFile("molephoto");
                                //String imageUrl = fileObject.getUrl();
                                //molephoto = Uri.parse(imageUrl);
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            bmp1 = decodeSampledBitmapFromResource(data, 500, 500);
                                        }
                                    }
                                });
                            }catch (NullPointerException n){}

                            try {
                                ParseFile fileObject = object.getParseFile("photo1");
                                //String imageUrl = fileObject.getUrl();
                                //photo1 = Uri.parse(imageUrl);
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            bmp2 = decodeSampledBitmapFromResource(data, 500, 500);
                                        }
                                    }
                                });
                            }catch (NullPointerException n){}

                            try {
                                ParseFile fileObject = object.getParseFile("photo2");
                                //String imageUrl = fileObject.getUrl();
                                //photo2 = Uri.parse(imageUrl);
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            bmp3 = decodeSampledBitmapFromResource(data, 500, 500);
                                        }
                                    }
                                });
                            }catch (NullPointerException n){}
                        }
                    }
                }
            });
        }catch (NullPointerException n){}

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Button_photo_next :
                moveNext();
                break;
            case R.id.Button_photo_prev :
                movePrev();
                break;
            default:
                break;
        }
    }
    public void moveNext(){
        if(current.equals("molephoto")){
            //imgLoader.DisplayImage(photo1.toString(),ImageView_photo);
            ImageView_photo.setImageBitmap(bmp2);
            current = "photo1";
            Button_photo_next.setVisibility(View.VISIBLE);
            Button_photo_prev.setVisibility(View.VISIBLE);
        }else if(current.equals("photo1")){
            //imgLoader.DisplayImage(photo2.toString(),ImageView_photo);
            ImageView_photo.setImageBitmap(bmp3);
                current = "photo2";
            if(avail) {
                Button_photo_next.setVisibility(View.VISIBLE);
            }else {
                Button_photo_next.setVisibility(View.INVISIBLE);
            }
            Button_photo_prev.setVisibility(View.VISIBLE);
        }else if(current.equals("photo2")){
            //imgLoader.DisplayImage(photo3.toString(),ImageView_photo);
                ImageView_photo.setImageBitmap(bmp4);
                current = "photo3";
            Button_photo_next.setVisibility(View.INVISIBLE);
            Button_photo_prev.setVisibility(View.VISIBLE);
        }
    }
    public void movePrev(){
        if(current.equals("photo1")){
            //imgLoader.DisplayImage(molephoto.toString(),ImageView_photo);
            ImageView_photo.setImageBitmap(bmp1);
            current = "molephoto";
            Button_photo_prev.setVisibility(View.INVISIBLE);
            Button_photo_next.setVisibility(View.VISIBLE);
        }else if(current.equals("photo2")){
            //imgLoader.DisplayImage(photo1.toString(),ImageView_photo);
            ImageView_photo.setImageBitmap(bmp2);
            current = "photo1";
            Button_photo_prev.setVisibility(View.VISIBLE);
            Button_photo_next.setVisibility(View.VISIBLE);
        }else if(current.equals("photo3")){
            //imgLoader.DisplayImage(photo2.toString(),ImageView_photo);
            ImageView_photo.setImageBitmap(bmp3);
            current = "photo2";
            Button_photo_prev.setVisibility(View.VISIBLE);
            Button_photo_next.setVisibility(View.VISIBLE);
        }
    }
}
