package com.gradlesol.firstcheck;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class confirm_passwordActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_confirm_continue;
    EditText EditText_confirm_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .edit()
                .putBoolean("isFirstLogin", true)
                .apply();

        EditText_confirm_password = (EditText)findViewById(R.id.EditText_confirm_password);

        Button_confirm_continue = (Button) findViewById(R.id.Button_confirm_continue);
        Button_confirm_continue.setOnClickListener(this);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Button_confirm_continue :
                if(validate()) {
                    if (EditText_confirm_password.getText().toString().equals("FCREG01")) {
                        startActivity(new Intent(getApplicationContext(), new_passwordActivity.class));
                    } else {
                        show_dialog("Incorrect Password","The password you have provided is not correct, please try again.","OK");
                    }
                }
                break;
            default:
                break;
        }
    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    public boolean validate(){
        if(isEmpty(EditText_confirm_password)){
            Toast.makeText(getApplicationContext(),"Enter Password",Toast.LENGTH_LONG).show();
            return false;
        }else {
            return true;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }
}
