package com.gradlesol.firstcheck;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Random;

public class start_consultationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView TextView_consultation_mole;
    TextView TextView_consultation_rash;
    TextView TextView_consultation_other;
    TextView TextView_consultation_what;

    String android_id;

    String caseNo;

    private DbHelper dbHelper;

    private static final String ALLOWED_CHARACTERS ="0123456789qwertyuiopasdfghjklzxcvbnm";

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_consultation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DbHelper(getApplicationContext());
        dbHelper.deleteRecord();

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        TextView_consultation_mole = (TextView)findViewById(R.id.TextView_consultation_mole);
        TextView_consultation_mole.setOnClickListener(this);
        TextView_consultation_rash = (TextView)findViewById(R.id.TextView_consultation_rash);
        TextView_consultation_rash.setOnClickListener(this);
        TextView_consultation_other = (TextView)findViewById(R.id.TextView_consultation_other);
        TextView_consultation_other.setOnClickListener(this);
        TextView_consultation_what = (TextView)findViewById(R.id.TextView_consultation_what);
        TextView_consultation_what.setOnClickListener(this);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.TextView_consultation_mole:
                start_consultation("Mole");
                break;

            case R.id.TextView_consultation_rash:
                start_consultation("Rash");
                break;

            case R.id.TextView_consultation_other:
                start_consultation("Other");
                break;

            case R.id.TextView_consultation_what:

                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View promptView = layoutInflater.inflate(R.layout.prompt1, null);
                TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
                TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
                Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

                AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
                builder.setView(promptView);
                builder.setCancelable(true);
                prompt1_title.setText("Information");
                prompt1_message.setText("\nDermatology is concerned with the diagnosis, treatment and prevention of diseases of the skin, hair, nails, oral cavity (mouth), genitals and is undertaken by a dermatologist. A dermatologist is sometimes also involved in cosmetic care and enhancement.");
                prompt1_button.setText("OK");
                final AlertDialog alert = builder.create();
                prompt1_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();
                break;

            default:
                break;
        }
    }

    public void start_consultation(final String type){

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);

        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(true);
        prompt1_title.setText("Instruction");
        prompt1_message.setText("\nYou are about to take multiple photos\nto send to your dermatologist. The\nphotos will be of the " +
                "same area of skin\nconcern but taken at different\ndistances.\n\nTo maintain privacy and anonymity,\nplease " +
                        "ensure you cannot be identified\nby the photos.\n\nTo proceed to take your first photo,\nclick continue, and" +
                        " follow the prompts.");
        prompt1_button.setText("Continue");
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.inserRecord(android_id,type);
                startActivity(new Intent(getApplicationContext(),takephotoActivity.class).putExtra("type","first").putExtra("sequence",true).putExtra("review",false));
                alert.dismiss();
            }
        });
        alert.show();
    }
}
