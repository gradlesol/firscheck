package com.gradlesol.firstcheck;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.onesignal.OneSignal;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class payment_successActivity extends AppCompatActivity {

    Button Button_paymntsuccess_finish;

    String caseNo,derm,name,dermuid,type,username,gender,age,howlong,description,region,ethnicity,code,label,insurance;
    String from;
    Boolean myrecords = false;

    private DbHelper dbHelper;
    private Cursor cursor;

    byte[] photo1, photo2, photo3, molephoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);

        /*dbHelper = new DbHelper(getApplicationContext());
        cursor = dbHelper.selectRecords();*/

        derm = getIntent().getStringExtra("derm");
        name = getIntent().getStringExtra("name");
        dermuid = getIntent().getStringExtra("dermuid");

        myrecords = getIntent().getBooleanExtra("myrecords",false);
        if(myrecords){
            caseNo = getIntent().getStringExtra("caseNo");
        }

        /*if(flag){
            caseNo = getIntent().getStringExtra("caseNo");
        }else {
            if (cursor.moveToFirst()) {
                do {
                    caseNo = cursor.getString(cursor.getColumnIndex("caseNo"));
                    gender = cursor.getString(cursor.getColumnIndex("gender"));
                    age = cursor.getString(cursor.getColumnIndex("age"));
                    howlong = cursor.getString(cursor.getColumnIndex("howlong"));
                    description = cursor.getString(cursor.getColumnIndex("description"));
                    region = cursor.getString(cursor.getColumnIndex("region"));
                    ethnicity = cursor.getString(cursor.getColumnIndex("ethnicity"));
                    code = cursor.getString(cursor.getColumnIndex("code"));
                    label = cursor.getString(cursor.getColumnIndex("label"));
                    insurance = cursor.getString(cursor.getColumnIndex("insurance"));
                    type = cursor.getString(cursor.getColumnIndex("type"));
                    username = cursor.getString(cursor.getColumnIndex("Username"));
                    photo1 = cursor.getBlob(cursor.getColumnIndex("photo1"));
                    photo2 = cursor.getBlob(cursor.getColumnIndex("photo2"));
                    photo3 = cursor.getBlob(cursor.getColumnIndex("photo3"));
                    molephoto = cursor.getBlob(cursor.getColumnIndex("molephoto"));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }*/

        Button_paymntsuccess_finish = (Button) findViewById(R.id.Button_paymntsuccess_finish);
        Button_paymntsuccess_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myrecords){
                    startActivity(new Intent(getApplicationContext(),uploadingActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .putExtra("derm",derm)
                            .putExtra("name",name)
                            .putExtra("dermuid",dermuid)
                            .putExtra("caseNo",caseNo)
                            .putExtra("myrecords",myrecords)
                            .putExtra("saved",false)
                    );
                }else {
                    startActivity(new Intent(getApplicationContext(),uploadingActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .putExtra("derm",derm)
                            .putExtra("name",name)
                            .putExtra("dermuid",dermuid)
                            .putExtra("myrecords",myrecords)
                            .putExtra("saved",false)
                    );
                }
                /*
                final ProgressDialog dialog = new ProgressDialog(payment_successActivity.this);
                dialog.setMessage("Please Wait\nWe keep your photos in high resolution and are uploading those to the server now. Please allow the upload to complete before closing.\nTo find out more about and to buy the Firstcheck SkinScope visit www.firstcheck.co.nz");
                dialog.show();
                if (flag) {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
                    query.whereEqualTo("caseNo", caseNo);
                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(final ParseObject object, ParseException e) {
                            object.put("saved",false);
                            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                                @Override
                                public void idsAvailable(String userId, String registrationId) {
                                    Log.d("debug", "User:" + userId);
                                    object.put("uid", userId);
                                    if (registrationId != null)
                                        Log.d("debug", "registrationId:" + registrationId);
                                }
                            });
                            object.put("derm", derm);
                            object.put("name", name);
                            object.put("completed", false);
                            object.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {

                                        try {
                                            OneSignal.postNotification(new JSONObject("{'contents': {'en':'You have a new case from a patient.'}, 'include_player_ids': ['" + dermuid + "']}"),
                                                    new OneSignal.PostNotificationResponseHandler() {
                                                        @Override
                                                        public void onSuccess(JSONObject response) {
                                                            Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                        }

                                                        @Override
                                                        public void onFailure(JSONObject response) {
                                                            Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                        }
                                                    });
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            finish();
                                        } catch (JSONException j) {
                                        }
                                    }
                                }
                            });

                        }
                    });
                } else {
                    final ParseObject object = new ParseObject(constants.TABLE_NAME);
                    object.put("caseNo", caseNo);
                    object.put("type", type);
                    object.put("username", username);
                    object.put("gender", gender);
                    object.put("age", age);
                    object.put("howlong", howlong);
                    object.put("description", description);
                    object.put("region", region);
                    object.put("ethnicity", ethnicity);
                    object.put("code", code);
                    object.put("label", label);
                    object.put("insurance", insurance);
                    String partFilename = "photo1" + ".jpg";
                    if (photo1 != null) {
                        final ParseFile photoFile = new ParseFile(partFilename, photo1);
                        object.put("photo1", photoFile);
                    }
                    if (photo2 != null) {
                        partFilename = "photo2" + ".jpg";
                        final ParseFile photoFile = new ParseFile(partFilename, photo2);
                        object.put("photo2", photoFile);
                    }
                    if (photo3 != null) {
                        partFilename = "photo3" + ".jpg";
                        final ParseFile photoFile = new ParseFile(partFilename, photo3);
                        object.put("photo3", photoFile);
                    }
                    if (molephoto != null) {
                        partFilename = "molephoto" + ".jpg";
                        final ParseFile photoFile = new ParseFile(partFilename, molephoto);
                        object.put("molephoto", photoFile);
                    }
                    object.put("saved", false);
                    OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                        @Override
                        public void idsAvailable(String userId, String registrationId) {
                            Log.d("debug", "User:" + userId);
                            object.put("uid", userId);
                            if (registrationId != null)
                                Log.d("debug", "registrationId:" + registrationId);
                        }
                    });
                    object.put("derm", derm);
                    object.put("name", name);
                    object.put("completed", false);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {

                                try {
                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'You have a new case from a patient.'}, 'include_player_ids': ['" + dermuid + "']}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                    dialog.dismiss();
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } catch (JSONException j) {
                                }
                            }
                        }
                    });
                }*/
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
