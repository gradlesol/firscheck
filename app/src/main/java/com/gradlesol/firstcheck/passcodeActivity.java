package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class passcodeActivity extends AppCompatActivity {

    EditText EditText_userpasscode;
    TextView TextView_userpasscode_back;
    Boolean FirstRun;
    Button Button_passcode_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passcode);
        getSupportActionBar().hide();

        checkFirstRunPasscode();

        Button_passcode_save = (Button)findViewById(R.id.Button_passcode_save);
        Button_passcode_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEmpty(EditText_userpasscode)){
                    Toast.makeText(getApplicationContext(),"Empty",Toast.LENGTH_LONG).show();
                } else {
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .edit()
                            .putString("FirstRunPasscode", EditText_userpasscode.getText().toString())
                            .apply();
                    startActivity(new Intent(getApplicationContext(),my_recordsActivity.class));
                }
            }
        });

        EditText_userpasscode = (EditText)findViewById(R.id.EditText_userpasscode);
        EditText_userpasscode.setHint("Passcode");

        TextView_userpasscode_back = (TextView)findViewById(R.id.TextView_userpasscode_back);
        TextView_userpasscode_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        if(FirstRun){
            Button_passcode_save.setVisibility(View.VISIBLE);
        }else{
            Button_passcode_save.setVisibility(View.INVISIBLE);
            final String password = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("FirstRunPasscode", null);
            EditText_userpasscode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (EditText_userpasscode.getText().toString().equals(password)) {

                        startActivity(new Intent(getApplicationContext(), my_recordsActivity.class));
                        finish();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }


    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    public void checkFirstRunPasscode() {
        String isFirstRun = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("FirstRunPasscode", null);
        if (isFirstRun == null){
            FirstRun = true;
        } else {
            FirstRun = false;
        }
    }
}
