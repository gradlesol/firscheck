package com.gradlesol.firstcheck;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetDataCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class register_dermActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_register_derm_register,Button_registerderm_select_regions;
    EditText EditText_registerderm_name,EditText_registerderm_qualification,EditText_registerderm_clinic_name,EditText_registerderm_clinic_location,EditText_registerderm_aemail;
    ImageView ImageView_registerderm;
    ParseFile photoFile = null;
    SwitchCompat SwitchCompat_registerderm_accept;
    TextView TextView_registerderm_region,TextView_registerderm_terms;

    Bitmap photo = null;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    String email,password;
    Boolean admin;

    String LoginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_derm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView_registerderm = (ImageView)findViewById(R.id.ImageView_registerderm);
        ImageView_registerderm.setOnClickListener(this);

        TextView_registerderm_region = (TextView)findViewById(R.id.TextView_registerderm_region);
        TextView_registerderm_terms = (TextView)findViewById(R.id.TextView_registerderm_terms);
        TextView_registerderm_terms.setOnClickListener(this);
        EditText_registerderm_name = (EditText)findViewById(R.id.EditText_registerderm_name);
        EditText_registerderm_qualification = (EditText)findViewById(R.id.EditText_registerderm_qualification);
        EditText_registerderm_clinic_name   = (EditText)findViewById(R.id.EditText_registerderm_clinic_name);
        EditText_registerderm_clinic_location = (EditText)findViewById(R.id.EditText_registerderm_clinic_location);
        EditText_registerderm_aemail = (EditText)findViewById(R.id.EditText_registerderm_clinic_aemail);

        Button_register_derm_register = (Button)findViewById(R.id.Button_register_derm_register);
        Button_register_derm_register.setOnClickListener(this);

        Button_registerderm_select_regions = (Button)findViewById(R.id.Button_registerderm_clinic_regions);
        Button_registerderm_select_regions.setOnClickListener(this);

        SwitchCompat_registerderm_accept = (SwitchCompat)findViewById(R.id.SwitchCompat_registerderm_accept);

        admin = getIntent().getBooleanExtra("admin",false);

        if(admin){
            LoginId = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginUsername", null);
            ParseUser parseUser = ParseUser.getCurrentUser();
            try {
                ParseFile fileObject = parseUser.getParseFile("Photo");
                fileObject.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            if(data != null)
                                photo = decodeSampledBitmapFromResource(data, 100, 100);
                                ImageView_registerderm.setImageBitmap(photo);
                        }
                    }
                });
            }catch (NullPointerException n){}

            List<String> user_regions = (ArrayList<String>) parseUser.get("regions");
            constants.region = user_regions;
            if(constants.region !=  null) {
                if (constants.region.size() != 0) {
                    String[] values = constants.region.toArray(new String[constants.region.size()]);
                    TextView_registerderm_region.setText(Arrays.toString(values).replaceAll("\\[|\\]", ""));
                }
            }else {
                constants.region = new ArrayList<String>();
            }
            EditText_registerderm_name.setText(parseUser.getString("Name"));
            EditText_registerderm_clinic_location.setText(parseUser.getString("Location"));
            EditText_registerderm_clinic_name.setText(parseUser.getString("Clinic"));
            EditText_registerderm_qualification.setText(parseUser.getString("Qualification"));
            EditText_registerderm_aemail.setText(parseUser.getString("apptEmail"));
            Button_register_derm_register.setText("Update");

        }else {
            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstLogin", true)
                    .apply();
            if(constants.region == null){
                constants.region = new ArrayList<String>();
            }else {
                if(constants.region.size() != 0)
                    constants.region.clear();
            }

            email = getIntent().getStringExtra("email");
            password = getIntent().getStringExtra("password");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Button_register_derm_register :
                if(validate()) {
                    if(SwitchCompat_registerderm_accept.isChecked()) {

                        if(admin){
                            ParseUser user = ParseUser.getCurrentUser();
                            String[] array = constants.region.toArray(new String[constants.region.size()]);
                            user.put("Name", EditText_registerderm_name.getText().toString());
                            user.put("Qualification", EditText_registerderm_qualification.getText().toString());
                            user.put("Clinic", EditText_registerderm_clinic_name.getText().toString());
                            user.put("Location", EditText_registerderm_clinic_location.getText().toString());
                            user.put("regions", Arrays.asList(array));
                            user.put("apptEmail",EditText_registerderm_aemail.getText().toString());
                            if(photo == null) {

                            }else {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                byte[] image = stream.toByteArray();
                                String partFilename = "dermpic.jpg";
                                photoFile = new ParseFile(partFilename, image);
                                try {
                                    user.put("Photo", photoFile);
                                }catch (NullPointerException ne){}
                            }
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e == null) {
                                        startActivity(new Intent(getApplicationContext(), your_casesActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                        finish();
                                    }else {
                                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                }
                            });


                        }else {
                            final ProgressDialog dialog = new ProgressDialog(register_dermActivity.this);
                            dialog.setMessage("Registering...");
                            dialog.show();

                            String[] array = constants.region.toArray(new String[constants.region.size()]);
                            final ParseUser user = new ParseUser();
                            user.setUsername(email);
                            user.setPassword(password);
                            user.setEmail(email);
                            user.put("Name", EditText_registerderm_name.getText().toString());
                            user.put("Qualification", EditText_registerderm_qualification.getText().toString());
                            user.put("Clinic", EditText_registerderm_clinic_name.getText().toString());
                            user.put("Location", EditText_registerderm_clinic_location.getText().toString());
                            user.put("regions", Arrays.asList(array));
                            user.put("apptEmail",EditText_registerderm_aemail.getText().toString());
                            user.signUpInBackground(new SignUpCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e == null){
                                        if(photo == null) {
                                            dialog.dismiss();
                                            display_screen();
                                        }else {
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                            byte[] image = stream.toByteArray();
                                            String partFilename = "dermpic.jpg";
                                            photoFile = new ParseFile(partFilename, image);
                                            try {
                                                user.put("Photo", photoFile);
                                                user.saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        dialog.dismiss();
                                                        display_screen();
                                                    }
                                                });
                                            }catch (NullPointerException ne){}
                                        }
                                    }else {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }

                        /*
                        ParseUser.logInInBackground(email, password, new LogInCallback() {
                            @Override
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    user.put("Name", EditText_registerderm_name.getText().toString());
                                    user.put("Qualification", EditText_registerderm_qualification.getText().toString());
                                    user.put("Clinic", EditText_registerderm_clinic_name.getText().toString());
                                    user.put("Location", EditText_registerderm_clinic_location.getText().toString());
                                    user.put("Photo", photoFile);
                                    user.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                ParseUser.logOut();
                                                dialog.dismiss();
                                                display_screen();
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Error :" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), "user not found", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });*/
                    }else {
                        show_dialog("Required","Please scroll down to accept the Terms & Conditions in order to continue.","OK");
                    }
                }
                break;

            case R.id.ImageView_registerderm :
                openImageChooser();
                break;
            case R.id.Button_registerderm_clinic_regions :
                startActivity(new Intent(getApplicationContext(),select_regionActivity.class));
                break;
            case R.id.TextView_registerderm_terms:
                startActivity(new Intent(getApplicationContext(),disclaimerActivity.class).putExtra("type","derms"));
                break;
            default:
                break;
        }
    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(constants.region == null){

        }else {
            if(constants.region.size() != 0){
                String[] values = constants.region.toArray(new String[constants.region.size()]);
                TextView_registerderm_region.setText(Arrays.toString(values).replaceAll("\\[|\\]", ""));
            }
        }
    }

    private void display_screen() {

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompt1, null);
            TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
            TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
            Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
            builder.setView(promptView);
            builder.setCancelable(false);
            prompt1_title.setText("Welcome");
            prompt1_message.setText("You are now registered as a Dermatologist");
            prompt1_button.setText("OK");
            final AlertDialog alert = builder.create();
            prompt1_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                }
            });
            alert.show();
    }

    void openImageChooser() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(register_dermActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }else if (items[item].equals("Choose from Library")) {

                    Intent intent = new Intent();
                    if (Build.VERSION.SDK_INT >= 19) {
                        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    } else {
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                    }
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == REQUEST_CAMERA) {
                photo = (Bitmap) data.getExtras().get("data");
            }else if(requestCode == SELECT_FILE){
                photo = null;
                if (data != null) {
                    try {
                        photo = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            ImageView_registerderm.setImageBitmap(photo);
        }
    }

    public boolean validate(){
        if(isEmpty(EditText_registerderm_name)){
            Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_LONG).show();
            return false;
        } else if(isEmpty(EditText_registerderm_qualification)){
            Toast.makeText(getApplicationContext(),"Enter Qualification",Toast.LENGTH_LONG).show();
            return false;
        } else if(isEmpty(EditText_registerderm_clinic_name)){
            Toast.makeText(getApplicationContext(),"Enter Clinic Name",Toast.LENGTH_LONG).show();
            return false;
        } else if(isEmpty(EditText_registerderm_clinic_location)){
            Toast.makeText(getApplicationContext(),"Enter Clinic Location",Toast.LENGTH_LONG).show();
            return false;
        } else if(constants.region.size() == 0){
            Toast.makeText(getApplicationContext(),"At Least You have to select any one region",Toast.LENGTH_LONG).show();
            return false;
        } else if(isEmpty(EditText_registerderm_aemail)){
            Toast.makeText(getApplicationContext(),"Enter Appointment Request Email",Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }

}
