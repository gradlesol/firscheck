package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.adapter.ExpandableListAdapter;
import com.gradlesol.firstcheck.adapter.dermatologistAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class export_dermActivity extends AppCompatActivity {

    ListView listView;
    protected List<ParseUser> mRecord;
    String Name="",region="";
    String derm_email_msg,email_msg,subject;
    Uri u1,u2,u3;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> tempDataHeader;
    HashMap<String, List<ParseUser>> listDataChild;
    Boolean avail = false;

    List<ParseUser> r0 = new ArrayList<ParseUser>();
    List<ParseUser> r1 = new ArrayList<ParseUser>();
    List<ParseUser> r2 = new ArrayList<ParseUser>();
    List<ParseUser> r3 = new ArrayList<ParseUser>();
    List<ParseUser> r4 = new ArrayList<ParseUser>();
    List<ParseUser> r5 = new ArrayList<ParseUser>();
    List<ParseUser> r6 = new ArrayList<ParseUser>();
    List<ParseUser> r7 = new ArrayList<ParseUser>();
    List<ParseUser> r8 = new ArrayList<ParseUser>();
    List<ParseUser> r9 = new ArrayList<ParseUser>();
    List<ParseUser> r10 = new ArrayList<ParseUser>();
    List<ParseUser> r11 = new ArrayList<ParseUser>();
    List<ParseUser> r12 = new ArrayList<ParseUser>();
    List<ParseUser> r13 = new ArrayList<ParseUser>();
    List<ParseUser> r14 = new ArrayList<ParseUser>();
    List<ParseUser> r15 = new ArrayList<ParseUser>();
    List<ParseUser> r16 = new ArrayList<ParseUser>();
    List<ParseUser> r17 = new ArrayList<ParseUser>();

    String[] strings = {
            "Northland Region",
            "Auckland Region",
            "Walkato Region",
            "Bay of Plenty Region",
            "Gisborne Region",
            "Hawke's Bay Region",
            "Taranaki Region",
            "Manawatu-Wanganui Region",
            "Wellington Region",
            "Tasman Region",
            "Nelson Region",
            "Marlborough Region",
            "West Coast Region",
            "Canterbury Region",
            "Otago Region",
            "Southland Region",
            "Chatham Islands Territory",
            "Area Outside Territorial Authority"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_derm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email_msg = getIntent().getStringExtra("email_msg");
        u1 = Uri.parse(getIntent().getStringExtra("u1"));
        u2 = Uri.parse(getIntent().getStringExtra("u2"));
        u3 = Uri.parse(getIntent().getStringExtra("u3"));
        subject = getIntent().getStringExtra("subject");

        region = getIntent().getStringExtra("region");

        if(region != null){
            String[] temp = new String[strings.length];
            int p = 0;
            for(int i = 0; i<temp.length ; i++){
                if(i == 0){
                    temp[0] = region;
                }else {
                    if(region.equals(strings[p])){
                        p++;
                        i--;
                    } else {
                        temp[i] = strings[p];
                        p++;
                    }
                }
            }
            strings = temp;
        }

        expListView = (ExpandableListView) findViewById(R.id.lvExp_export_derm);
        prepareListData();

        /*
        listView = (ListView)findViewById(R.id.listView_export);

        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {

                if(e == null){
                    if(objects == null){
                        Toast.makeText(getApplicationContext(),"Object not found",Toast.LENGTH_LONG).show();
                    }else {
                        mRecord = objects;
                        try {
                            dermatologistAdapter adapter = new dermatologistAdapter(getApplicationContext(), mRecord);
                            listView.setAdapter(adapter);
                        } catch (NullPointerException n) {
                        }
                    }
                }else {

                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseUser recordObject = mRecord.get(position);
                Name = recordObject.getString("Name");
                derm_email_msg = "I would like to please be contacted regarding making an appointment to see Dr "+Name;
                derm_email_msg = derm_email_msg + "\n\nMy Full Name:\nMy date of birth:\nMy address:\nMy contact phone numbers:\nMy medical insurance:(Southern Cross / NIB / Other / None)(delete all but applicable)\n\n\n";
                derm_email_msg = derm_email_msg + email_msg;
                Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
                email.setType("text/jpeg");
                email.putExtra(Intent.EXTRA_EMAIL,new String[] {recordObject.getEmail()});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, derm_email_msg);
                ArrayList<Uri> uris = new ArrayList<Uri>();
                uris.add(u1);
                uris.add(u2);
                uris.add(u3);
                if(uris != null) {
                    email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                }
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                finish();
            }
        });
        */
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Boolean prepareListData() {
        listDataHeader = new ArrayList<String>();
        tempDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<ParseUser>>();
        if(setData()){
            return true;
        }else {
            return false;
        }
    }

    public Boolean setChild(){
        Collections.sort(listDataHeader);
        if(avail){
            tempDataHeader.add(region.toUpperCase());
            for(int i=0 ;i < listDataHeader.size(); i++ ){
                if((region).equalsIgnoreCase(listDataHeader.get(i))){

                }else {
                    tempDataHeader.add(listDataHeader.get(i));
                }
            }
            listDataHeader = tempDataHeader;
        }

        int n = 0;
        for(int i=0; i<18; i++){
            if(n < listDataHeader.size()) {
                if ((i == 0) && (r0.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r0);
                    n++;
                } else if ((i == 1) && (r1.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r1);
                    n++;
                } else if ((i == 2) && (r2.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r2);
                    n++;
                } else if ((i == 3) && (r3.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r3);
                    n++;
                } else if ((i == 4) && (r4.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r4);
                    n++;
                } else if ((i == 5) && (r5.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r5);
                    n++;
                } else if ((i == 6) && (r6.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r6);
                    n++;
                } else if ((i == 7) && (r7.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r7);
                    n++;
                } else if ((i == 8) && (r8.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r8);
                    n++;
                } else if ((i == 9) && (r9.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r9);
                    n++;
                } else if ((i == 10) && (r10.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r10);
                    n++;
                } else if ((i == 11) && (r11.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r11);
                    n++;
                } else if ((i == 12) && (r12.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r12);
                    n++;
                } else if ((i == 13) && (r13.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r13);
                    n++;
                } else if ((i == 14) && (r14.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r14);
                    n++;
                } else if ((i == 15) && (r15.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r15);
                    n++;
                } else if ((i == 16) && (r16.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r16);
                    n++;
                } else if ((i == 17) && (r17.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r17);
                    n++;
                } else {
                    //do nothing
                }
            }
            if(i == 17){
                listAdapter = new ExpandableListAdapter(getApplicationContext(), listDataHeader, listDataChild);
                expListView.setAdapter(listAdapter);
                for(int l=0; l < listAdapter.getGroupCount(); l++)
                    expListView.expandGroup(l);

                expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        return true;
                    }
                });
                expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        ParseUser user = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                        Name = user.getString("Name");
                        derm_email_msg = "I would like to please be contacted regarding making an appointment to see Dr "+Name;
                        derm_email_msg = derm_email_msg + "\n\nMy Full Name:\nMy date of birth:\nMy address:\nMy contact phone numbers:\nMy medical insurance:(Southern Cross / NIB / Other / None)(delete all but applicable)\n\n\n";
                        derm_email_msg = derm_email_msg + email_msg;
                        Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
                        email.setType("text/jpeg");
                        email.putExtra(Intent.EXTRA_EMAIL,new String[] {user.getString("apptEmail")});
                        email.putExtra(Intent.EXTRA_SUBJECT, subject);
                        email.putExtra(Intent.EXTRA_TEXT, derm_email_msg);
                        ArrayList<Uri> uris = new ArrayList<Uri>();
                        uris.add(u1);
                        uris.add(u2);
                        uris.add(u3);
                        if(uris != null) {
                            email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                        }
                        startActivity(Intent.createChooser(email, "Choose an Email client :"));
                        finish();
                        return false;
                    }
                });
            }
        }

        return true;
    }

    public Boolean setData(){
        if(isNetworkAvailable()){
            try {
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        if (e == null) {
                            try {
                                if (objects.size() == 0) {
                                    Toast.makeText(getApplicationContext(), "No Records found", Toast.LENGTH_LONG).show();
                                } else {
                                    for(int i=0 ; i < objects.size() ; i++){
                                        ParseObject UserObject = objects.get(i);
                                        if(UserObject.get("regions") != null) {
                                            List<String> user_regions = (ArrayList<String>) UserObject.get("regions");
                                            for (int j = 0; j < user_regions.size(); j++) {
                                                for (int k = 0; k < strings.length; k++) {
                                                    if (strings[k].equals(user_regions.get(j))) {
                                                        addtolist(objects.get(i), k);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    setChild();
                                }

                            } catch (NullPointerException n) {
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }catch (NullPointerException n){

            }
        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void addtolist(ParseUser object,int k){
        setHeader(k);

        if(region != null) {
            if (strings[k].equals(region)) {
                avail = true;
            }
        }
        switch (k) {
            case 0:
                r0.add(object);
                break;
            case 1:
                r1.add(object);
                break;
            case 2:
                r2.add(object);
                break;
            case 3:
                r3.add(object);
                break;
            case 4:
                r4.add(object);
                break;
            case 5:
                r5.add(object);
                break;
            case 6:
                r6.add(object);
                break;
            case 7:
                r7.add(object);
                break;
            case 8:
                r8.add(object);
                break;
            case 9:
                r9.add(object);
                break;
            case 10:
                r10.add(object);
                break;
            case 11:
                r11.add(object);
                break;
            case 12:
                r12.add(object);
                break;
            case 13:
                r13.add(object);
                break;
            case 14:
                r14.add(object);
                break;
            case 15:
                r15.add(object);
                break;
            case 16:
                r16.add(object);
                break;
            case 17:
                r17.add(object);
                break;
            default:
                break;
        }
    }

    public void setHeader(int k){
        String HeaderItem = strings[k].toUpperCase();
        Boolean find = false;
        if(listDataHeader.size() == 0){
            listDataHeader.add(HeaderItem);
            find = true;
        }else {
            for (int i = 0; i < listDataHeader.size(); i++) {
                if (HeaderItem.equals(listDataHeader.get(i))) {
                    find = true;
                    break;
                }
            }
        }
        if(!find){
            listDataHeader.add(HeaderItem);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
