package com.gradlesol.firstcheck;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

public class BuyerskinscopeActivity extends AppCompatActivity implements View.OnClickListener {
    TextView TextView_buyerskinscope_terms,
            TextView_buyerskinscope_onefor,
            TextView_buyerskinscope_twofor,
            TextView_buyerskinscope_threefor;

    EditText EditText_buyerskinscope_name,
            EditText_buyerskinscope_email,
            EditText_buyerskinscope_number,
            EditText_buyerskinscope_address;

    Button Button_buyer_register;

    SwitchCompat SwitchCompat_buyerskinscope_accept;

    String price = "30";

    int quantity = 1;

    private static final String TAG = "paymentExample";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    //sandbox//private static final String CONFIG_CLIENT_ID = "AY9MvS6CbBDAqtI9T8qHOZ85FqZrUt6o5XvZZGf4XfAAv74h5aD0EIwlGrJhskJVISmx8WJzMkUTMZ9N";
    private static final String CONFIG_CLIENT_ID = "Aft_6NzptYYtXC9o7M3ItDLo8MZAr9d2oWbdPLdah5xCm2-9GOML-pyemK8Bn_dsDV82eThP0LpQy5lf";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("firstcheck")
            .merchantPrivacyPolicyUri(Uri.parse("http://firstcheck.airsquare.com/privacy-policy.cfm"))
            .merchantUserAgreementUri(Uri.parse("http://firstcheck.airsquare.com/privacy-policy.cfm"));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyerskinscope);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView_buyerskinscope_terms=(TextView)findViewById(R.id.TextView_buyerskinscope_terms);
        TextView_buyerskinscope_onefor=(TextView)findViewById(R.id.TextView_buyerskinscope_onefor);
        TextView_buyerskinscope_twofor=(TextView)findViewById(R.id.TextView_buyerskinscope_twofor);
        TextView_buyerskinscope_threefor=(TextView)findViewById(R.id.TextView_buyerskinscope_threefor);

        EditText_buyerskinscope_name = (EditText)findViewById(R.id.EditText_buyerskinscope_name);
        EditText_buyerskinscope_email = (EditText)findViewById(R.id.EditText_buyerskinscope_email);
        EditText_buyerskinscope_number = (EditText)findViewById(R.id.EditText_buyerskinscope_number);
        EditText_buyerskinscope_address = (EditText)findViewById(R.id.EditText_buyerskinscope_address);

        TextView_buyerskinscope_onefor.setOnClickListener(this);
        TextView_buyerskinscope_twofor.setOnClickListener(this);
        TextView_buyerskinscope_threefor.setOnClickListener(this);
        TextView_buyerskinscope_terms.setOnClickListener(this);

        Button_buyer_register = (Button)findViewById(R.id.Button_buyer_register);
        Button_buyer_register.setOnClickListener(this);

        SwitchCompat_buyerskinscope_accept = (SwitchCompat)findViewById(R.id.SwitchCompat_buyerskinscope_accept);
/*
        TextView_buyerskinscope_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),terms_displayActivity.class));
            }
        });
        */
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean validate(){
        if(isEmpty(EditText_buyerskinscope_name)){
            Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_LONG).show();
            return false;
        }else if(isEmpty(EditText_buyerskinscope_email)){
            Toast.makeText(getApplicationContext(),"Enter Email",Toast.LENGTH_LONG).show();
            return false;
        }else if(isEmpty(EditText_buyerskinscope_number)){
            Toast.makeText(getApplicationContext(),"Enter Phone Number",Toast.LENGTH_LONG).show();
            return false;
        }else if(isEmpty(EditText_buyerskinscope_address)){
            Toast.makeText(getApplicationContext(),"Enter Address",Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.TextView_buyerskinscope_onefor :

                TextView_buyerskinscope_onefor.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_buyerskinscope_onefor.setBackgroundResource(R.drawable.rounded_tab3);

                TextView_buyerskinscope_twofor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_twofor.setBackgroundResource(R.drawable.square_tab1);

                TextView_buyerskinscope_threefor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_threefor.setBackgroundResource(R.drawable.rounded_tab2);

                price = "30";
                quantity = 1;
                break;
            case R.id.TextView_buyerskinscope_twofor :

                TextView_buyerskinscope_twofor.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_buyerskinscope_twofor.setBackgroundResource(R.drawable.square_tab2);

                TextView_buyerskinscope_onefor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_onefor.setBackgroundResource(R.drawable.rounded_tab1);

                TextView_buyerskinscope_threefor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_threefor.setBackgroundResource(R.drawable.rounded_tab2);

                price="55";
                quantity = 2;
                break;
            case R.id.TextView_buyerskinscope_threefor :

                TextView_buyerskinscope_threefor.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_buyerskinscope_threefor.setBackgroundResource(R.drawable.rounded_tab4);

                TextView_buyerskinscope_twofor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_twofor.setBackgroundResource(R.drawable.square_tab1);

                TextView_buyerskinscope_onefor.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_buyerskinscope_onefor.setBackgroundResource(R.drawable.rounded_tab1);

                price = "70";
                quantity = 3;
                break;
            case R.id.TextView_buyerskinscope_terms:
                startActivity(new Intent(getApplicationContext(),show_termsActivity.class));
                break;
            case R.id.Button_buyer_register :
                if(validate()){
                    if(SwitchCompat_buyerskinscope_accept.isChecked()) {
                        onBuyPressed();
                    }else {
                        show_dialog("Required","\nPlease scroll down to accept the Terms & Conditions in order to continue.","OK");
                    }
                }
                break;
            default:
                break;
        }

    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }
    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(BuyerskinscopeActivity.this, PaymentActivity.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(price), "USD","SkinScope Price",
                paymentIntent);
    }

    protected void displayResultText(String result) {
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObj = new JSONObject(confirm.toJSONObject().toString());
                        String id  = jsonObj.getJSONObject("response").getString("id");
                        String status = jsonObj.getJSONObject("response").getString("state");

                        displayResultText("PaymentConfirmation info received from PayPal");
                        ParseObject object = new ParseObject("SkinScope");
                        object.put("name",EditText_buyerskinscope_name.getText().toString());
                        object.put("phone",EditText_buyerskinscope_number.getText().toString());
                        object.put("paypalID",id);
                        object.put("status",status);
                        object.put("address",EditText_buyerskinscope_address.getText().toString());
                        object.put("email",EditText_buyerskinscope_email.getText().toString());
                        object.put("quantity",quantity);
                        object.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                startActivity(new Intent(getApplicationContext(), Spayment_successActivity.class));
                                finish();
                            }
                        });

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");
                        startActivity(new Intent(getApplicationContext(),payment_successActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }
    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
