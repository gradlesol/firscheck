package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class login_dermatologistActivity extends AppCompatActivity implements View.OnClickListener {

    Button Button_login_derm_Register,Button_login_derm_Login;
    EditText EditText_loginderm_username,EditText_loginderm_password;
    TextView TextView_loginderm_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_dermatologist);
        getSupportActionBar().hide();

        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .edit()
                .putString("LoginUsername", null)
                .putString("LoginPassword",null)
                .apply();

        EditText_loginderm_username = (EditText)findViewById(R.id.EditText_loginderm_username);
        EditText_loginderm_password = (EditText)findViewById(R.id.EditText_loginderm_password);

        Button_login_derm_Login = (Button)findViewById(R.id.Button_login_derm_Login);
        Button_login_derm_Login.setOnClickListener(this);

        Button_login_derm_Register = (Button)findViewById(R.id.Button_login_derm_Register);
        Button_login_derm_Register.setOnClickListener(this);

        TextView_loginderm_back = (TextView)findViewById(R.id.TextView_loginderm_back);
        TextView_loginderm_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.Button_login_derm_Login:
                if(validate()) {
                    if(isNetworkAvailable()){
                            ParseUser.logInInBackground(EditText_loginderm_username.getText().toString(), EditText_loginderm_password.getText().toString(), new LogInCallback() {
                                public void done(final ParseUser user, ParseException e) {
                                    if (user != null) {
                                        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                                            @Override
                                            public void idsAvailable(String userId, String registrationId) {
                                                Log.d("debug", "User:" + userId);
                                                if (registrationId != null)
                                                    Log.d("debug", "registrationId:" + registrationId);
                                                user.put("uid", userId);
                                            }
                                        });
                                        user.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                                            .edit()
                                                            .putString("LoginUsername", user.getUsername())
                                                            .putString("LoginPassword", EditText_loginderm_password.getText().toString())
                                                            .apply();
                                                    startActivity(new Intent(getApplicationContext(), your_casesActivity.class));
                                                    finish();
                                                }
                                            }
                                        });
                                    } else {
                                        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                                .edit()
                                                .putString("LoginUsername", null)
                                                .putString("LoginPassword", null)
                                                .apply();
                                    }
                                    if (e != null) {
                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                    }else {
                        Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.Button_login_derm_Register :
                if(isNetworkAvailable()){
                        startActivity(new Intent(getApplicationContext(),confirm_passwordActivity.class));
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.TextView_loginderm_back :
                onBackPressed();
                finish();
                break;
            default:
                break;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean validate(){
        if(isEmpty(EditText_loginderm_username)){
         Toast.makeText(getApplicationContext(),"Enter User Name",Toast.LENGTH_LONG).show();
            return false;
        }else if(isEmpty(EditText_loginderm_password)){
            Toast.makeText(getApplicationContext(),"Enter Password",Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }
}
