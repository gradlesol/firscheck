package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gradlesol.firstcheck.adapter.detailsAdapter;
import com.gradlesol.firstcheck.fragment.completedFragment;
import com.gradlesol.firstcheck.fragment.pendingFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.net.InetAddress;
import java.util.List;

public class your_casesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView TextView_your_cases_pending,TextView_your_cases_completed,TextView_your_cases_logout,TextView_your_cases_options;

    /*
    ListView listView;
    protected List<ParseObject> mRecord;
*/
    String user;
    //Boolean completed = false;


    pendingFragment pendingfragment;
    completedFragment completedfragment;

    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_cases);
        getSupportActionBar().hide();

        user = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginUsername", null);

        if(user == null){
            startActivity(new Intent(getApplicationContext(),login_dermatologistActivity.class));
            finish();
        }

        TextView_your_cases_pending = (TextView)findViewById(R.id.TextView_your_cases_pending);
        TextView_your_cases_pending.setOnClickListener(this);

        TextView_your_cases_completed = (TextView)findViewById(R.id.TextView_your_cases_completed);
        TextView_your_cases_completed.setOnClickListener(this);

        TextView_your_cases_options = (TextView)findViewById(R.id.TextView_your_cases_options);
        TextView_your_cases_options.setOnClickListener(this);

        TextView_your_cases_logout = (TextView)findViewById(R.id.TextView_your_cases_logout);
        TextView_your_cases_logout.setOnClickListener(this);

        pendingfragment = new pendingFragment();
        completedfragment = new completedFragment();

        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_your_cases, pendingfragment);
        fragmentTransaction.commit();

        /*
        if(completed){
            TextView_your_cases_completed.setTextColor(Color.parseColor("#FFFFFF"));
            TextView_your_cases_completed.setBackgroundColor(getResources().getColor(R.color.greenButton));
            TextView_your_cases_pending.setTextColor(getResources().getColor(R.color.greenButton));
            TextView_your_cases_pending.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        if(isNetworkAvailable()){
                try {
                    listView = (ListView) findViewById(R.id.dlistview);
                    ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);
                    query.whereEqualTo("completed", completed);
                    query.whereEqualTo("derm", user);
                    query.orderByDescending("createdAt");
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {

                            if (e == null) {
                                try {
                                    mRecord = objects;
                                    detailsAdapter adapter = new detailsAdapter(getApplicationContext(), mRecord);
                                    listView.setAdapter(adapter);
                                } catch (NullPointerException n) {
                                }
                            } else {

                            }
                        }
                    });

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ParseObject recordObject = mRecord.get(position);
                            String caseNo = recordObject.getString("caseNo");
                            startActivity(new Intent(getApplicationContext(), case_detailsActivity.class).putExtra("caseNo", caseNo).putExtra("completed", completed));
                        }
                    });
                }catch (NullPointerException n){}

        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }
        */

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        int id = item.getItemId();

        if(id == R.id.action_logout){
                // search action
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("\nDo you want to logout?\n")
                        .setCancelable(false)
                        .setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ParseUser.logOut();
                                startActivity(new Intent(getApplicationContext(),MainActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.TextView_your_cases_pending :
                TextView_your_cases_pending.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_your_cases_pending.setBackgroundResource(R.drawable.rounded_tab3);
                TextView_your_cases_completed.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_your_cases_completed.setBackgroundResource(R.drawable.rounded_tab2);

                fm = getSupportFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_your_cases, pendingfragment);
                fragmentTransaction.commit();

                /*
                completed = false;

                if(isNetworkAvailable()){
                        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);
                        query.whereEqualTo("derm",user);
                        query.whereEqualTo("completed",false);
                        query.orderByDescending("createdAt");
                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {

                                if(e == null){
                                    try {
                                        mRecord = objects;
                                        detailsAdapter adapter = new detailsAdapter(getApplicationContext(), mRecord);
                                        listView.setAdapter(adapter);
                                    }catch (NullPointerException n){}
                                }else {

                                }
                            }
                        });
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                }
                */
                break;
            case R.id.TextView_your_cases_completed :
                TextView_your_cases_completed.setTextColor(Color.parseColor("#FFFFFF"));
                TextView_your_cases_completed.setBackgroundResource(R.drawable.rounded_tab4);
                TextView_your_cases_pending.setTextColor(getResources().getColor(R.color.greenButton));
                TextView_your_cases_pending.setBackgroundResource(R.drawable.rounded_tab1);

                fm = getSupportFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_your_cases, completedfragment);
                fragmentTransaction.commit();
                /*
                completed = true;
                if(isNetworkAvailable()){
                        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);

                        query.whereEqualTo("derm",user);
                        query.whereEqualTo("completed",true);
                        query.orderByDescending("createdAt");
                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {

                                if(e == null){
                                    try {
                                        mRecord = objects;
                                        detailsAdapter adapter = new detailsAdapter(getApplicationContext(), mRecord);
                                        listView.setAdapter(adapter);
                                    }catch (NullPointerException n){}
                                }else {

                                }
                            }
                        });

                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
                }*/
                break;
            case R.id.TextView_your_cases_logout :

                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View promptView = layoutInflater.inflate(R.layout.prompt5, null);
                TextView prompt5_title = (TextView) promptView.findViewById(R.id.prompt5_title);
                TextView prompt5_message = (TextView) promptView.findViewById(R.id.prompt5_message);
                Button prompt5_negative = (Button)promptView.findViewById(R.id.prompt5_negative);
                Button prompt5_positive = (Button)promptView.findViewById(R.id.prompt5_positive);

                AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
                builder.setView(promptView);
                builder.setCancelable(true);
                prompt5_title.setText("Do you want to logout?");
                prompt5_message.setText("");
                prompt5_positive.setText("Logout");
                prompt5_negative.setText("Cancel");
                final AlertDialog alert = builder.create();
                prompt5_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ParseUser.logOut();
                        startActivity(new Intent(getApplicationContext(),MainActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }
                });
                prompt5_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();

                break;
            case R.id.TextView_your_cases_options :
                startActivity(new Intent(getApplicationContext(),AdminOptionActivity.class));
                break;
            default:
                break;
        }
    }
}
