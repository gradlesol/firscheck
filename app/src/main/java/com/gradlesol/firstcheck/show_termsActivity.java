package com.gradlesol.firstcheck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class show_termsActivity extends AppCompatActivity {

    Button Button_show_terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_terms);
        getSupportActionBar().setTitle("Terms & Conditions");
        WebView wv= (WebView)findViewById(R.id.webView_show_terms);
        wv.loadUrl("file:///android_asset/TermsSkinScope.htm");

        Button_show_terms = (Button)findViewById(R.id.Button_show_terms);
        Button_show_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
    }
}
