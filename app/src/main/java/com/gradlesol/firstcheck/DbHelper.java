package com.gradlesol.firstcheck;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static String CREATE_TABLE;
    static String DATABASE_NAME = "firstcheckDB";

    public static final String TABLE_Cases = "Cases";
    public static final String ctype = "type";
    public static final String cphoto1 = "photo1";
    public static final String cphoto2 = "photo2";
    public static final String cphoto3 = "photo3";
    public static final String cmolephoto = "molephoto";
    public static final String cUsername = "Username";
    public static final String ccaseNo = "caseNo";
    public static final String cgender = "gender";
    public static final String cage = "age";
    public static final String chowlong = "howlong";
    public static final String cdescription = "description";
    public static final String cregion = "region";
    public static final String cethnicity = "ethnicity";
    public static final String ccode = "code";
    public static final String clabel = "label";
    public static final String cinsurance = "insurance";
    public static final String cdiagnosis = "diagnosis";
    public static final String creviewDate = "reviewedDate";

    private ContentValues cValues;
    private SQLiteDatabase dataBase = null;
    private Cursor cursor;

    public DbHelper(Context context) {
        super(context, context.getExternalFilesDir(null).getAbsolutePath()
                + "/" + DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CREATE_TABLE = "CREATE TABLE " + TABLE_Cases + "(" + cUsername
                + " TEXT PRIMARY KEY, "
                + ctype + " TEXT, "
                + cphoto1 + " BLOB, "
                + cphoto2 + " BLOB, "
                + cphoto3 + " BLOB, "
                + cmolephoto + " BLOB, "
                + ccaseNo + " TEXT, "
                + cgender + " TEXT, "
                + cage + " TEXT, "
                + chowlong + " TEXT, "
                + cdescription + " TEXT, "
                + cregion + " TEXT, "
                + cethnicity + " TEXT, "
                + ccode + " TEXT, "
                + clabel + " TEXT, "
                + creviewDate + " TEXT, "
                + cdiagnosis + " TEXT, "
                + cinsurance + " TEXT)";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Cases);
        onCreate(db);
    }


    public void inserRecord(String Username,String type) {

        dataBase = getWritableDatabase();
        cValues = new ContentValues();

        cValues.put(cUsername, Username);
        cValues.put(ctype,type);

        dataBase.insert(TABLE_Cases, null, cValues);

        dataBase.close();
    }

    public void updateRecord(String columnname, byte[] image) {

        dataBase = getWritableDatabase();

        cValues = new ContentValues();

        cValues.put(columnname, image);

        dataBase.update(DbHelper.TABLE_Cases, cValues,
                null, null);

        dataBase.close();
    }

    public void updateData(String columnname, String data) {

        dataBase = getWritableDatabase();

        cValues = new ContentValues();

        cValues.put(columnname, data);

        dataBase.update(DbHelper.TABLE_Cases, cValues,
                null, null);

        dataBase.close();
    }

    public Cursor selectRecords() {

        dataBase = getReadableDatabase();

        cursor = dataBase.rawQuery("select * from " + TABLE_Cases, null);
        return cursor;
    }

    public void deleteRecord() {

        dataBase = getWritableDatabase();

        dataBase.delete(TABLE_Cases, null, null);

        dataBase.close();
    }

}
