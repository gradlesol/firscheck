package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class new_passwordActivity extends AppCompatActivity {

    Button Button_new_password_continue;
    EditText EditText_newpassword_password,EditText_newpassword_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .edit()
                .putBoolean("isFirstLogin", true)
                .apply();

        EditText_newpassword_password = (EditText)findViewById(R.id.EditText_newpassword_password);
        EditText_newpassword_email = (EditText)findViewById(R.id.EditText_newpassword_email);

        Button_new_password_continue = (Button) findViewById(R.id.Button_new_password_continue);
        Button_new_password_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()) {
                    //if(checkFirstRunDermDisclaimer()) {
                        final String password = EditText_newpassword_password.getText().toString();
                        final String email = EditText_newpassword_email.getText().toString();

                    ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                    userQuery.whereEqualTo("email",email);
                    userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser object, ParseException e) {
                            if(object == null) {
                                startActivity(new Intent(getApplicationContext(), register_dermActivity.class).putExtra("email",email).putExtra("password", password).putExtra("admin",false));
                            }else {
                                Toast.makeText(getApplicationContext(), "This email address already registered", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    /*
                        final ParseUser user = new ParseUser();
                        user.setUsername(email);
                        user.setPassword(password);
                        user.setEmail(email);
                        user.signUpInBackground(new SignUpCallback() {
                            public void done(ParseException e) {
                                if (e == null) {
                                    startActivity(new Intent(getApplicationContext(), register_dermActivity.class).putExtra("password", password));
                                } else {
                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });*/
                    //}
                }
            }
        });

    }

    public boolean checkFirstRunDermDisclaimer() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getBoolean("isFirstRunDermDisclaimer", true);
        if (isFirstRun) {
            startActivity(new Intent(getApplicationContext(), disclaimerActivity.class).putExtra("type","derm"));
            getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRunDermDisclaimer", false)
                    .apply();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean validate(){
        if(isEmpty(EditText_newpassword_email)){
            Toast.makeText(getApplicationContext(),"Enter Email Address",Toast.LENGTH_LONG).show();
            return false;
        }else if(isEmpty(EditText_newpassword_password)){
            Toast.makeText(getApplicationContext(),"Enter Password",Toast.LENGTH_LONG).show();
            return false;
        }else  {
            return true;
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }
}
