package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gradlesol.firstcheck.fragment.aboutFragment;
import com.gradlesol.firstcheck.fragment.faqFragment;
import com.gradlesol.firstcheck.fragment.homeFragment;
import com.gradlesol.firstcheck.fragment.our_dermatologistsFragment;
import com.gradlesol.firstcheck.fragment.privacyFragment;
import com.gradlesol.firstcheck.fragment.skinscopeFragment;
import com.gradlesol.firstcheck.fragment.terms_condiFragment;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        Fabric.with(this, new Crashlytics());

        checkFirstRunDialog();

        homeFragment main = new homeFragment();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_main, main);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        int id = item.getItemId();
        if(id == R.id.action_flag){
            // search action
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null)
                if (info.getState() == NetworkInfo.State.CONNECTED)
                {
                    return true;
                }
        }
        return false;


    }
    public void checkWiFiFirstRunNotification() {
            if (isConnectingToInternet() == false) {
                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View promptView = layoutInflater.inflate(R.layout.prompt5, null);
                TextView prompt5_title = (TextView) promptView.findViewById(R.id.prompt5_title);
                TextView prompt5_message = (TextView) promptView.findViewById(R.id.prompt5_message);
                Button prompt5_negative = (Button)promptView.findViewById(R.id.prompt5_negative);
                Button prompt5_positive = (Button)promptView.findViewById(R.id.prompt5_positive);

                AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
                builder.setView(promptView);
                builder.setCancelable(true);
                prompt5_title.setText("WIFI Not Detected");
                prompt5_message.setText("\nTo start a new consultation we suggest that you are connected to a WIFI network to upload your photos in full resolution");
                prompt5_positive.setText("Use My Data");
                prompt5_negative.setText("Cancel");
                final AlertDialog alert = builder.create();
                prompt5_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(),start_consultationActivity.class);
                        startActivity(i);
                        alert.dismiss();
                    }
                });
                prompt5_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();
            } else {
                Intent i = new Intent(getApplicationContext(),start_consultationActivity.class);
                startActivity(i);
            }
    }

    public void checkFirstRunDialog() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getBoolean("isFirstRunDialog", true);
        if (isFirstRun){
            first_dialog();
            getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRunDialog", false)
                    .apply();
        } else {

        }
    }

    public void first_dialog(){

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);

        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText("Welcome to Firstcheck");
        prompt1_message.setText("\nThis app is still in a final testing stage and has been released in connection with Firstcheck’s Pledgeme Project campaign. Please visit our campaign page at www.pledgeme.co.nz (search “Firstcheck”) for more information and before proceeding.");
        prompt1_button.setText("Continue");
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            item.setChecked(false);

            getSupportActionBar().setTitle("Home");

            homeFragment main = new homeFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_our_dermatologists) {
            item.setChecked(false);

            getSupportActionBar().setTitle("Our Dermatologists");

            our_dermatologistsFragment main = new our_dermatologistsFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_faq) {
            item.setChecked(false);

            getSupportActionBar().setTitle("FAQ");

            faqFragment main = new faqFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_terms_condi) {
            item.setChecked(false);

            getSupportActionBar().setTitle("Terms & Conditions");
            terms_condiFragment main = new terms_condiFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_tell) {
            item.setChecked(false);
            try
            { Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Firstcheck");
                String sAux = "\nLet me recommend you this application\n\n";
                sAux = sAux + "https://play.google.com/store/apps/details?id=com.gradlesol.firstcheck \n\n";
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            }
            catch(Exception e)
            {
                Toast.makeText(getApplicationContext(),"Error :"+e.toString(),Toast.LENGTH_LONG).show();
            }

        } else if (id == R.id.nav_about) {
            item.setChecked(false);

            getSupportActionBar().setTitle("About");

            aboutFragment main = new aboutFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_my_records) {
            item.setChecked(false);

            Intent i = new Intent(getApplicationContext(),passcodeActivity.class);
            i.putExtra("user","anonymous");
            startActivity(i);

        } else if (id == R.id.nav_start_consultation) {
            item.setChecked(false);

            checkFirstRunDisclaimer();


        } else if (id == R.id.nav_privacy) {
            item.setChecked(false);

            getSupportActionBar().setTitle("Privacy Policy");
            privacyFragment main = new privacyFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_dermatologist) {
            item.setChecked(false);

            String user = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginUsername", null);
                if (user != null) {
                    startActivity(new Intent(getApplicationContext(),dpasscodeActivity.class));
                } else {
                    startActivity(new Intent(getApplicationContext(),login_dermatologistActivity.class));
                }


        } else if (id == R.id.nav_skinscope) {
            item.setChecked(true);

            getSupportActionBar().setTitle("SkinScope");
            skinscopeFragment main = new skinscopeFragment();

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_main, main);
            fragmentTransaction.commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {

    }

    public void checkFirstRunDisclaimer() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getBoolean("isFirstRunDisclaimer", true);
        if (isFirstRun){
            startActivity(new Intent(getApplicationContext(),disclaimerActivity.class).putExtra("type","case"));
            getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRunDisclaimer", false)
                    .apply();
        } else {
            checkWiFiFirstRunNotification();
        }
    }

}
