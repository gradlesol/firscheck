package com.gradlesol.firstcheck.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.adapter.ExpandableListAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class our_dermatologistsFragment extends Fragment {


    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<ParseUser>> listDataChild;

    List<ParseUser> r0 = new ArrayList<ParseUser>();
    List<ParseUser> r1 = new ArrayList<ParseUser>();
    List<ParseUser> r2 = new ArrayList<ParseUser>();
    List<ParseUser> r3 = new ArrayList<ParseUser>();
    List<ParseUser> r4 = new ArrayList<ParseUser>();
    List<ParseUser> r5 = new ArrayList<ParseUser>();
    List<ParseUser> r6 = new ArrayList<ParseUser>();
    List<ParseUser> r7 = new ArrayList<ParseUser>();
    List<ParseUser> r8 = new ArrayList<ParseUser>();
    List<ParseUser> r9 = new ArrayList<ParseUser>();
    List<ParseUser> r10 = new ArrayList<ParseUser>();
    List<ParseUser> r11 = new ArrayList<ParseUser>();
    List<ParseUser> r12 = new ArrayList<ParseUser>();
    List<ParseUser> r13 = new ArrayList<ParseUser>();
    List<ParseUser> r14 = new ArrayList<ParseUser>();
    List<ParseUser> r15 = new ArrayList<ParseUser>();
    List<ParseUser> r16 = new ArrayList<ParseUser>();
    List<ParseUser> r17 = new ArrayList<ParseUser>();

    String[] strings = {
            "Northland Region",
            "Auckland Region",
            "Walkato Region",
            "Bay of Plenty Region",
            "Gisborne Region",
            "Hawke's Bay Region",
            "Taranaki Region",
            "Manawatu-Wanganui Region",
            "Wellington Region",
            "Tasman Region",
            "Nelson Region",
            "Marlborough Region",
            "West Coast Region",
            "Canterbury Region",
            "Otago Region",
            "Southland Region",
            "Chatham Islands Territory",
            "Area Outside Territorial Authority"
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_our_dermatologists,container,false);

        Arrays.sort(strings);

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp_fragment_dermatologist);
        prepareListData();

        return view;
    }

    private Boolean prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<ParseUser>>();
        if(setData()){
            return true;
        }else {
            return false;
        }
    }

    public Boolean setChild(){
        Collections.sort(listDataHeader);
        int n = 0;
        for(int i=0; i<18; i++){
            if(n < listDataHeader.size()) {
                if ((i == 0) && (r0.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r0);
                    n++;
                } else if ((i == 1) && (r1.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r1);
                    n++;
                } else if ((i == 2) && (r2.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r2);
                    n++;
                } else if ((i == 3) && (r3.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r3);
                    n++;
                } else if ((i == 4) && (r4.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r4);
                    n++;
                } else if ((i == 5) && (r5.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r5);
                    n++;
                } else if ((i == 6) && (r6.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r6);
                    n++;
                } else if ((i == 7) && (r7.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r7);
                    n++;
                } else if ((i == 8) && (r8.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r8);
                    n++;
                } else if ((i == 9) && (r9.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r9);
                    n++;
                } else if ((i == 10) && (r10.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r10);
                    n++;
                } else if ((i == 11) && (r11.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r11);
                    n++;
                } else if ((i == 12) && (r12.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r12);
                    n++;
                } else if ((i == 13) && (r13.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r13);
                    n++;
                } else if ((i == 14) && (r14.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r14);
                    n++;
                } else if ((i == 15) && (r15.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r15);
                    n++;
                } else if ((i == 16) && (r16.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r16);
                    n++;
                } else if ((i == 17) && (r17.size() != 0)) {
                    listDataChild.put(listDataHeader.get(n), r17);
                    n++;
                } else {
                    //do nothing
                }
            }
            if(i == 17){
                listAdapter = new ExpandableListAdapter(getActivity().getApplicationContext(), listDataHeader, listDataChild);
                expListView.setAdapter(listAdapter);
                for(int l=0; l < listAdapter.getGroupCount(); l++)
                    expListView.expandGroup(l);

                expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        return true;
                    }
                });
            }
        }

        return true;
    }

    public Boolean setData(){
        if(isNetworkAvailable()){
            try {
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        if (e == null) {
                            try {
                                if (objects.size() == 0) {
                                    Toast.makeText(getActivity().getApplicationContext(), "No Records found", Toast.LENGTH_LONG).show();
                                } else {
                                    Log.d("STATUS","object get");
                                    for(int i=0 ; i < objects.size() ; i++){
                                        ParseObject UserObject = objects.get(i);
                                        if(UserObject.get("regions") != null) {
                                            List<String> user_regions = (ArrayList<String>) UserObject.get("regions");
                                            for (int j = 0; j < user_regions.size(); j++) {
                                                for (int k = 0; k < strings.length; k++) {
                                                    if (strings[k].equals(user_regions.get(j))) {
                                                        Log.d("STATUS", "object add to list");
                                                        addtolist(objects.get(i), k);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    Log.d("STATUS","final add list completed");
                                    setChild();
                                }

                            } catch (NullPointerException n) {
                            }
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }catch (NullPointerException n){

            }
        }else {
            Toast.makeText(getActivity().getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void addtolist(ParseUser object,int k){
        setHeader(k);
        switch (k){
            case 0 : r0.add(object);
                break;
            case 1 : r1.add(object);
                break;
            case 2 : r2.add(object);
                break;
            case 3 : r3.add(object);
                break;
            case 4 : r4.add(object);
                break;
            case 5 : r5.add(object);
                break;
            case 6 : r6.add(object);
                break;
            case 7 : r7.add(object);
                break;
            case 8 : r8.add(object);
                break;
            case 9 : r9.add(object);
                break;
            case 10 : r10.add(object);
                break;
            case 11 : r11.add(object);
                break;
            case 12 : r12.add(object);
                break;
            case 13 : r13.add(object);
                Log.d("STATUS","r13");
                break;
            case 14 : r14.add(object);
                break;
            case 15 : r15.add(object);
                break;
            case 16 : r16.add(object);
                break;
            case 17 : r17.add(object);
                break;
            default:
                break;
        }
        Log.d("STATUS", "list added complete");
    }

    public void setHeader(int k){
        String HeaderItem = strings[k].toUpperCase();
        Boolean find = false;
        if(listDataHeader.size() == 0){
            listDataHeader.add(HeaderItem);
            find = true;
        }else {
            for (int i = 0; i < listDataHeader.size(); i++) {
                if (HeaderItem.equals(listDataHeader.get(i))) {
                    find = true;
                    break;
                }
            }
        }
        if(find == false){
            listDataHeader.add(HeaderItem);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
