package com.gradlesol.firstcheck.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.adapter.details_savedAdapter;
import com.gradlesol.firstcheck.constants;
import com.gradlesol.firstcheck.my_records_details_Activity;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Gradle on 04-Aug-16.
 */
public class savedFragment extends Fragment {

    ListView listView;
    protected List<ParseObject> mRecord;

    String android_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saved,container,false);

        android_id = Settings.Secure.getString(getContext().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        listView = (ListView)view.findViewById(R.id.fragment_saved_listview);

        if(isNetworkAvailable()){
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);
            query.whereEqualTo("username",android_id);
            query.orderByDescending("createdAt");
            query.whereEqualTo("saved",true);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if(objects == null){
                        Toast.makeText(getContext().getApplicationContext(),"No Records Found",Toast.LENGTH_LONG).show();
                    }else {
                        if (e == null) {
                            try {
                                mRecord = objects;
                                details_savedAdapter adapter = new details_savedAdapter(getContext().getApplicationContext(), mRecord);
                                listView.setAdapter(adapter);
                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        ParseObject recordObject = mRecord.get(position);
                                        String caseNo = recordObject.getString("caseNo");
                                        startActivity(new Intent(getContext().getApplicationContext(),my_records_details_Activity.class).putExtra("caseNo",caseNo));
                                    }
                                });
                            } catch (NullPointerException n) {
                            }
                        } else {

                        }
                    }
                }
            });
        }else {
            Toast.makeText(getContext().getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }



        return view;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
