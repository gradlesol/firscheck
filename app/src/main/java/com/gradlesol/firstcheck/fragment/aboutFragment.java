package com.gradlesol.firstcheck.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.gradlesol.firstcheck.R;

public class aboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_about,container,false);

        WebView wv= (WebView) view.findViewById(R.id.webView_about);
        wv.loadUrl("file:///android_asset/about.htm");

        return view;
    }
}
