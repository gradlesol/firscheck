package com.gradlesol.firstcheck.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.adapter.detailsAdapter;
import com.gradlesol.firstcheck.case_detailsActivity;
import com.gradlesol.firstcheck.constants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Gradle on 04-Aug-16.
 */

public class completedFragment extends Fragment {

    String user;

    ListView listView;
    protected List<ParseObject> mRecord;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_completed,container,false);

        user = getActivity().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getString("LoginUsername", null);

        listView = (ListView)view.findViewById(R.id.fragment_completed_listview);

        if(isNetworkAvailable()){
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(constants.TABLE_NAME);
                query.whereEqualTo("completed", true);
                query.whereEqualTo("derm", user);
                query.orderByDescending("createdAt");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {

                        if(objects == null){

                        }else {
                            if (e == null) {
                                mRecord = objects;
                                detailsAdapter adapter = new detailsAdapter(getContext().getApplicationContext(), mRecord);
                                listView.setAdapter(adapter);
                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        ParseObject recordObject = mRecord.get(position);
                                        String caseNo = recordObject.getString("caseNo");
                                        startActivity(new Intent(getActivity().getApplicationContext(), case_detailsActivity.class)
                                                .putExtra("caseNo", caseNo)
                                                .putExtra("completed", true));
                                    }
                                });
                            } else {

                            }
                        }
                    }
                });

        }else {
            Toast.makeText(getActivity().getApplicationContext(),"No Internet Connected.",Toast.LENGTH_LONG).show();
        }

        return view;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
