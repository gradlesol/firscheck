package com.gradlesol.firstcheck.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gradlesol.firstcheck.R;
import com.gradlesol.firstcheck.disclaimerActivity;
import com.gradlesol.firstcheck.passcodeActivity;
import com.gradlesol.firstcheck.start_consultationActivity;

public class homeFragment extends Fragment implements View.OnClickListener {

    Button Button_home_new_consultation, Button_home_my_records;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);

        Button_home_new_consultation = (Button) view.findViewById(R.id.Button_home_new_consultation);
        Button_home_new_consultation.setOnClickListener(this);

        Button_home_my_records = (Button) view.findViewById(R.id.Button_home_my_records);
        Button_home_my_records.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.Button_home_new_consultation:
                checkFirstRunDisclaimer();
                break;

            case R.id.Button_home_my_records:

                Intent i = new Intent(getContext().getApplicationContext(), passcodeActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }
    }

    public void checkFirstRunDisclaimer() {
        boolean isFirstRun = getActivity().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).getBoolean("isFirstRunDisclaimer", true);
        if (isFirstRun) {
            startActivity(new Intent(getContext().getApplicationContext(), disclaimerActivity.class).putExtra("type","case"));
            getActivity().getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRunDisclaimer", false)
                    .apply();
        } else {
            checkWiFiFirstRunNotification();
        }
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public void checkWiFiFirstRunNotification() {
            if (isConnectingToInternet() == false) {
                LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
                View promptView = layoutInflater.inflate(R.layout.prompt5, null);
                TextView prompt5_title = (TextView) promptView.findViewById(R.id.prompt5_title);
                TextView prompt5_message = (TextView) promptView.findViewById(R.id.prompt5_message);
                Button prompt5_negative = (Button)promptView.findViewById(R.id.prompt5_negative);
                Button prompt5_positive = (Button)promptView.findViewById(R.id.prompt5_positive);

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext(),R.style.CustomDialog);
                builder.setView(promptView);
                builder.setCancelable(true);
                prompt5_title.setText("WIFI Not Detected");
                prompt5_message.setText("\nTo start a new consultation we suggest that you are connected to a WIFI network to upload your photos in full resolution");
                prompt5_positive.setText("Use My Data");
                prompt5_negative.setText("Cancel");
                final AlertDialog alert = builder.create();
                prompt5_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity().getApplicationContext(),start_consultationActivity.class);
                        startActivity(i);
                        alert.dismiss();
                    }
                });
                prompt5_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                alert.show();
            } else {
                Intent i = new Intent(getActivity().getApplicationContext(), start_consultationActivity.class);
                startActivity(i);
            }
}
}
