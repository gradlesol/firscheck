package com.gradlesol.firstcheck;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class my_records_details_Activity extends AppCompatActivity implements View.OnClickListener {

    ImageView ImageView_my_records_details_molephoto,ImageView_my_records_details_photo1,ImageView_my_records_details_photo2,ImageView_my_records_details_photo3;
    TextView TextView_my_records_details_userData;
    TextView EditText_my_records_details_desciption;
    TextView TextView_my_records_details_ReviewedBy;
    ImageView ImageView_myrecords_details_que;
    TextView export_derm;
    Button Button_my_records_details_export;
    byte[] molephoto,photo1,photo2,photo3;

    String caseNo;
    String subject,email_msg,region="";

    TextView EditText_my_records_details_reviewedDate,EditText_my_records_details_response;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_records_details_);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        caseNo = getIntent().getStringExtra("caseNo");

        ImageView_my_records_details_molephoto = (ImageView)findViewById(R.id.ImageView_my_records_details_molephoto);
        ImageView_my_records_details_molephoto.setOnClickListener(this);

        ImageView_my_records_details_photo1 = (ImageView)findViewById(R.id.ImageView_my_records_details_photo1);
        ImageView_my_records_details_photo1.setOnClickListener(this);

        ImageView_my_records_details_photo2 = (ImageView)findViewById(R.id.ImageView_my_records_details_photo2);
        ImageView_my_records_details_photo2.setOnClickListener(this);

        ImageView_my_records_details_photo3 = (ImageView)findViewById(R.id.ImageView_my_records_details_photo3);
        ImageView_my_records_details_photo3.setOnClickListener(this);

        ImageView_myrecords_details_que = (ImageView)findViewById(R.id.ImageView_myrecords_details_que);
        ImageView_myrecords_details_que.setOnClickListener(this);

        Button_my_records_details_export = (Button)findViewById(R.id.Button_my_records_details_export);
        Button_my_records_details_export.setOnClickListener(this);

        TextView_my_records_details_ReviewedBy = (TextView)findViewById(R.id.TextView_my_records_details_ReviewedBy);
        export_derm = (TextView)findViewById(R.id.export_derm);
        export_derm.setOnClickListener(this);

        TextView_my_records_details_userData = (TextView)findViewById(R.id.TextView_my_records_details_userData);
        EditText_my_records_details_desciption = (TextView)findViewById(R.id.EditText_my_records_details_description);
        EditText_my_records_details_reviewedDate = (TextView)findViewById(R.id.EditText_my_records_details_reviewedDate);
        EditText_my_records_details_response = (TextView)findViewById(R.id.EditText_my_records_details_response);

        try {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(constants.TABLE_NAME);
            query.whereEqualTo("caseNo", caseNo);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (object == null) {

                    } else {
                        if (e == null) {

                            String userData = "Date: " + object.getCreatedAt().toString();
                            userData = userData + "\n\nType: " + object.getString("type");
                            userData = userData + "\n\nLabel: " + object.getString("label");
                            userData = userData + "\n\nReference Code: " + object.getString("caseNo");
                            userData = userData + "\n\nGender: " + object.getString("gender");
                            userData = userData + "\n\nAge: " + object.getString("age");
                            userData = userData + "\n\nMedical Insurance: " + object.getString("insurance");
                            userData = userData + "\n\nHealth Practitioner Code: " + object.getString("code");
                            userData = userData + "\n\nEthnicity: " + object.getString("ethnicity");
                            userData = userData + "\n\nRegion: " + object.getString("region");
                            region = object.getString("region");
                            email_msg = userData;
                            subject = caseNo + " NZ Dermatology & skin Cancer Center";
                            email_msg = email_msg + "\n\nHow long have you had it: "+ object.getString("howlong");
                            userData = userData + "\n\nHow Long: " + object.getString("howlong");
                            TextView_my_records_details_userData.setText(userData);

                            EditText_my_records_details_desciption.setText(object.getString("description"));
                            email_msg = email_msg + "\n\nYour Description: "+object.getString("description");

                            if(object.getString("name") == null){
                                email_msg = email_msg +  "\n\nReviewed By:";
                            }else {
                                email_msg = email_msg + "\n\nReviewed By:" + object.getString("name");
                                TextView_my_records_details_ReviewedBy.setText("Reviewed By: " + object.getString("name"));
                            }
                            if(object.getDate("reviewedDate") == null){
                                email_msg = email_msg +  "\n" + "Review Date:";
                            }else {
                                EditText_my_records_details_reviewedDate.setText("Reviewed: " + object.getDate("reviewedDate").toString());
                                email_msg = email_msg + "\nReview Date:" + EditText_my_records_details_reviewedDate.getText().toString();
                            }
                            EditText_my_records_details_response.setText(object.getString("diagnosis"));
                            email_msg = email_msg + "\nResponse:" + EditText_my_records_details_response.getText().toString() + "\n";

                            try {
                                ParseFile fileObject = object.getParseFile("molephoto");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            molephoto = data;
                                            ImageView_my_records_details_molephoto.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo1");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            photo1 = data;
                                            ImageView_my_records_details_photo1.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo2");
                                fileObject.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if (e == null) {
                                            photo2 = data;
                                            ImageView_my_records_details_photo2.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                        }
                                    }
                                });
                                fileObject = object.getParseFile("photo3");
                                if(fileObject !=  null) {
                                    ImageView_my_records_details_photo3.setVisibility(View.VISIBLE);
                                    fileObject.getDataInBackground(new GetDataCallback() {
                                        @Override
                                        public void done(byte[] data, ParseException e) {
                                            if (e == null) {
                                                photo3 = data;
                                                ImageView_my_records_details_photo3.setImageBitmap(decodeSampledBitmapFromResource(data, 70, 70));
                                            }
                                        }
                                    });
                                }
                            }catch (Exception e1){}
                        }
                    }
                }
            });
        }catch (NullPointerException n){}


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.ImageView_my_records_details_molephoto :
                startActivity(new Intent(getApplicationContext(),photoActivity.class).putExtra("type","molephoto").putExtra("caseNo",caseNo));
                break;
            case R.id.ImageView_my_records_details_photo1 :
                startActivity(new Intent(getApplicationContext(),photoActivity.class).putExtra("type","photo1").putExtra("caseNo",caseNo));
                break;
            case R.id.ImageView_my_records_details_photo2 :
                startActivity(new Intent(getApplicationContext(),photoActivity.class).putExtra("type","photo2").putExtra("caseNo",caseNo));
                break;
            case R.id.ImageView_my_records_details_photo3 :
                startActivity(new Intent(getApplicationContext(),photoActivity.class).putExtra("type","photo3").putExtra("caseNo",caseNo));
                break;
            case R.id.ImageView_myrecords_details_que:
                show_dialog("Information","\nAfter choosing your dermatologist, a draft email will be created in your email program for completing and sending to the dermatologist with your contact details.\n\nYou will then become identifiable to the dermatologist.","OK");
                break;

            case R.id.Button_my_records_details_export:
                Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
                email.setType("text/jpeg");
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, email_msg);
                ArrayList<Uri> uris = new ArrayList<Uri>();
                if(molephoto != null) {
                    Uri u1 = Uri.fromFile(generatPhoto(molephoto, "molephoto"));
                    uris.add(u1);
                }
                if(photo1 != null) {
                    Uri u2 = Uri.fromFile(generatPhoto(photo1, "photo1"));
                    uris.add(u2);
                }
                if(photo2 != null) {
                    Uri u3 = Uri.fromFile(generatPhoto(photo2, "photo2"));
                    uris.add(u3);
                }
                if(uris != null) {
                    email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                }
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                break;
            case R.id.export_derm:
                uris = new ArrayList<Uri>();
                Uri u1 = Uri.fromFile(generatPhoto(molephoto,"molephoto"));
                uris.add(u1);
                Uri u2 = Uri.fromFile(generatPhoto(photo1,"photo1"));
                uris.add(u2);
                Uri u3 = Uri.fromFile(generatPhoto(photo2,"photo2"));
                uris.add(u3);
                startActivity(new Intent(getApplicationContext(),export_dermActivity.class)
                        .putExtra("subject",subject)
                        .putExtra("email_msg",email_msg)
                        .putExtra("u1",u1.toString())
                        .putExtra("u2",u2.toString())
                        .putExtra("u3",u3.toString())
                        .putExtra("region",region)
                );
                break;
            default:
                break;

        }


    }

    public void show_dialog(String title,String message,String button){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.prompt1, null);
        TextView prompt1_title = (TextView) promptView.findViewById(R.id.prompt1_title);
        TextView prompt1_message = (TextView) promptView.findViewById(R.id.prompt1_message);
        Button prompt1_button = (Button)promptView.findViewById(R.id.prompt1_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
        builder.setView(promptView);
        builder.setCancelable(false);
        prompt1_title.setText(title);
        prompt1_message.setText(message);
        prompt1_button.setText(button);
        final AlertDialog alert = builder.create();
        prompt1_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    public File generatPhoto(byte[] data, String type){

        File pictureFile = getOutputMediaFile(type);
        if (pictureFile == null) {
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {
        }
        return pictureFile;
    }
    private static File getOutputMediaFile(String type) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_"+ type+"_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(byte[] image,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(image,0,image.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(image,0,image.length, options);
    }
}
