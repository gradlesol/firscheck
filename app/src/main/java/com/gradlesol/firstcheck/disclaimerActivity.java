package com.gradlesol.firstcheck;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseUser;

public class disclaimerActivity extends AppCompatActivity {

    Button Button_disclaimer_agree;
    String type = "case";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        type = getIntent().getStringExtra("type");

        Button_disclaimer_agree = (Button)findViewById(R.id.Button_disclaimer_agree);

        WebView wv= (WebView)findViewById(R.id.webView_disclaimer);
        if (type.equals("case")){
            wv.loadUrl("file:///android_asset/pdisclaimer.htm");}
        else {
            wv.loadUrl("file:///android_asset/ddisclaimer.htm");
            Button_disclaimer_agree.setText("Agree");
        }
        Button_disclaimer_agree = (Button)findViewById(R.id.Button_disclaimer_agree);
        Button_disclaimer_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("case")){
                    checkWiFiFirstRunNotification();
                }else {
                    finish();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
    }

    public void checkWiFiFirstRunNotification() {
        if (isConnectingToInternet() == false) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.prompt5, null);
            TextView prompt5_title = (TextView) promptView.findViewById(R.id.prompt5_title);
            TextView prompt5_message = (TextView) promptView.findViewById(R.id.prompt5_message);
            Button prompt5_negative = (Button)promptView.findViewById(R.id.prompt5_negative);
            Button prompt5_positive = (Button)promptView.findViewById(R.id.prompt5_positive);

            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomDialog);
            builder.setView(promptView);
            builder.setCancelable(true);
            prompt5_title.setText("WIFI Not Detected");
            prompt5_message.setText("\nTo start a new consultation we suggest that you are connected to a WIFI network to upload your photos in full resolution");
            prompt5_positive.setText("Use My Data");
            prompt5_negative.setText("Cancel");
            final AlertDialog alert = builder.create();
            prompt5_positive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(),start_consultationActivity.class);
                    startActivity(i);
                    alert.dismiss();
                    finish();
                }
            });
            prompt5_negative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                    finish();
                }
            });
            alert.show();
        } else {
            Intent i = new Intent(getApplicationContext(),start_consultationActivity.class);
            startActivity(i);
            finish();
        }
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null)
                if (info.getState() == NetworkInfo.State.CONNECTED)
                {
                    return true;
                }
        }
        return false;


    }
}
